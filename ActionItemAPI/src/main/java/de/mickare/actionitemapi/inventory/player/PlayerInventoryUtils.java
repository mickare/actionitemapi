package de.mickare.actionitemapi.inventory.player;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import de.mickare.actionitemapi.actionitem.ActionItemManager;

public final class PlayerInventoryUtils {

	private PlayerInventoryUtils() {
	}

	public static void renderItemStackToPlayerInventory( Inventory inv, Integer index, ItemStack itemstack, boolean doReplaceRendering ) {
		ItemStack old = null;
		if (!doReplaceRendering) {
			old = inv.getItem( index );
		}
		
		inv.setItem( index, itemstack );
		
		if(old != null) {
			inv.addItem(old);
		}
	}
	
	public static void renderItemStackToPlayerInventory( PlayerInventory inv, int index, ItemStack itemstack, boolean doReplaceRendering ) {
		ItemStack old = null;
		if (!doReplaceRendering) {
			old = inv.getItem( index );
		}
		
		inv.setItem( index, itemstack );
		
		if(old != null) {
			inv.addItem(old);
		}
	}
	
	public static void removeItemStackFromPlayerInventory( PlayerInventory inv, int index, ItemStack itemstack ) {
		ItemStack old = inv.getItem( index );
		if (old != null) {
			if (itemstack.equals( old )) {
				inv.setItem( index, null );
			}
		}
	}

	public static Map<Integer, ItemStack> removeAllWithMetaKeyFromPlayerInventory(PlayerInventory inv, String metakey) {
		HashMap<Integer, ItemStack> result = new HashMap<Integer, ItemStack>();
		for(int index = 0; index < inv.getSize(); index++) {
			ItemStack current = inv.getItem(index);
			if(current != null) {
				String key = ActionItemManager.getMetaKey(current);
				if(key != null) {
					if(metakey.equals(key)) {
						inv.setItem(index, null);
						result.put(index, current);
					}
				}
			}
		}
		return result;
 		
	}

	

}
