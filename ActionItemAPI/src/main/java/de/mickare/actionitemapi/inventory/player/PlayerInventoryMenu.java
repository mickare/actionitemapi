package de.mickare.actionitemapi.inventory.player;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.ActionItemManager;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryManager;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryMenu;
import de.mickare.actionitemapi.api.inventory.actionitem.PlayerInventoryActionItem;
import de.mickare.actionitemapi.inventory.AbstractInventoryMenu;
import de.mickare.actionitemapi.inventory.player.handler.RenderHandler;

public class PlayerInventoryMenu extends
		AbstractInventoryMenu<IPlayerInventoryManager, IPlayerInventoryMenu, PlayerInventoryActionItem> implements
		IPlayerInventoryMenu {
	
	public static final RenderHandler DEFAULT_RENDERHANDLER = new RenderHandler() {
		
		@Override
		public boolean onRender( PlayerInventoryMenu menu, Player player, int index, ItemStack itemstack,
				boolean doReplaceRendering ) {
			return true;
		}
		
	};
	
	private final HashMap<Integer, ItemStack> itemstacks = new HashMap<Integer, ItemStack>();
	private final Set<Player> players = Collections.newSetFromMap( new WeakHashMap<Player, Boolean>() );
	
	private boolean doReplaceRendering = true;
	
	private RenderHandler onRenderHandler = DEFAULT_RENDERHANDLER;
	
	public PlayerInventoryMenu( IPlayerInventoryManager manager, JavaPlugin plugin ) {
		this( manager, plugin, true );
	}
	
	public PlayerInventoryMenu( IPlayerInventoryManager manager, JavaPlugin plugin, boolean doReplaceRendering ) {
		super( manager, plugin );
		this.doReplaceRendering = doReplaceRendering;
	}
	
	// ****************************************************************
	// Getter / Setter
	
	public String[] getPlayers() {
		return players.toArray( new String[0] );
	}
	
	public boolean isDoReplaceRendering() {
		return doReplaceRendering;
	}
	
	public void setDoReplaceRendering( boolean doReplaceRendering ) {
		this.doReplaceRendering = doReplaceRendering;
	}
	
	@Override
	public PlayerInventoryActionItem _newItem( int id ) {
		return new PlayerInventoryActionItem( id, this );
	}
	
	@Override
	public PlayerInventoryActionItem setItem( int index, int id, ItemStack itemstack ) {
		PlayerInventoryActionItem item = this.getActionItem( id );
		if ( item == null ) {
			item = newItem( id );
		}
		setItem( index, item, itemstack );
		return item;
	}
	
	@Override
	public PlayerInventoryActionItem setItem( int index, PlayerInventoryActionItem actionitem, ItemStack itemstack ) {
		actionitem.addMetaKey( itemstack );
		ItemStack old = itemstacks.put( index, itemstack );
		if ( old != null ) {
			renderItemStackToAllPlayers( index, itemstack, true );
		} else {
			renderItemStackToAllPlayers( index, itemstack );
		}
		if ( !this.contains( actionitem ) ) {
			addActionItem( actionitem );
			ActionItemAPI.getActionItemManager().registerActionItem( actionitem );
		}
		return actionitem;
	}
	
	@Override
	public void setItem( int index, ItemStack itemstack ) {
		setItem( index, itemstack, true );
	}
	
	public void setItem( int index, ItemStack itemstack, boolean doReplaceRendering ) {
		this.itemstacks.put( index, itemstack );
		renderItemStackToAllPlayers( index, itemstack, doReplaceRendering );
	}
	
	@Override
	public boolean contains( String actionkey ) {
		return this.hasActionItem( actionkey );
	}
	
	@Override
	public boolean contains( ActionItem actionitem ) {
		return this.hasActionItem( actionitem );
	}
	
	@Override
	public PlayerInventoryActionItem[] getItems() {
		return this.getActionItems().toArray( new PlayerInventoryActionItem[0] );
	}
	
	@Override
	public void removeItem( ActionItem item ) {
		removeItem( item.getKey() );
	}
	
	@Override
	public void removeItem( String metakey ) {
		this.removeActionItem( metakey );
		HashMap<Integer, ItemStack> removeThese = new HashMap<Integer, ItemStack>();
		for ( Entry<Integer, ItemStack> e : itemstacks.entrySet() ) {
			if ( metakey.equals( ActionItemManager.getMetaKey( e.getValue() ) ) ) {
				removeThese.put( e.getKey(), e.getValue() );
			}
		}
		for ( Entry<Integer, ItemStack> d : removeThese.entrySet() ) {
			itemstacks.remove( d.getKey() );
			
		}
		removeAllWithMetaKeyFromAllPlayer( metakey );
	}
	
	@Override
	public void removeItem( int metaid ) {
		removeItem( ActionItemManager.generateMetaKey( metaid ) );
	}
	
	@Override
	public void clear() {
		this.clearActionItems();
		itemstacks.clear();
	}
	
	@Override
	public void clear( int index ) {
		ItemStack old = itemstacks.remove( index );
		if ( old != null ) {
			String metakey = ActionItemManager.getMetaKey( old );
			if ( metakey != null ) {
				
				boolean last = true;
				
				for ( ItemStack is : itemstacks.values() ) {
					if ( metakey.equals( ActionItemManager.getMetaKey( is ) ) ) {
						last = false;
						break;
					}
				}
				
				if ( last ) {
					this.removeActionItem( metakey );
				}
			}
		}
	}
	
	public boolean hasPlayer( Player player ) {
		return hasPlayer( player.getUniqueId() );
	}
	
	public boolean hasPlayer( UUID uuid ) {
		return players.contains( uuid );
	}
	
	public void addPlayer( Player player ) {
		getManager().removePlayerFromAll( player );
		if ( players.add( player ) ) {
			this.renderAllItemStacksToPlayer( player );
		}
	}
	
	public void removePlayer( Player player ) {
		if ( players.remove( player.getUniqueId() ) ) {
			this.removeAllItemStacksFromPlayer( player );
		}
	}
	
	// ****************************************************************
	// Rendering
	
	public void renderItemStackToPlayer( Player player, int index, ItemStack itemstack ) {
		renderItemStackToPlayer( player, index, itemstack, doReplaceRendering );
	}
	
	public void renderItemStackToPlayer( Player player, int index, ItemStack itemstack, boolean doReplaceRendering ) {
		if ( onRenderHandler.onRender( this, player, index, itemstack, doReplaceRendering ) ) {
			PlayerInventoryUtils
					.renderItemStackToPlayerInventory( player.getInventory(), index, itemstack, doReplaceRendering );
		}
	}
	
	public void renderAllItemStacksToPlayer( Player player ) {
		renderAllItemStacksToPlayer( player, doReplaceRendering );
	}
	
	public void renderAllItemStacksToPlayer( Player player, boolean doReplaceRendering ) {
		for ( Entry<Integer, ItemStack> ise : itemstacks.entrySet() ) {
			renderItemStackToPlayer( player, ise.getKey(), ise.getValue(), doReplaceRendering );
		}
	}
	
	public void renderItemStackToAllPlayers( int index, ItemStack itemstack ) {
		renderItemStackToAllPlayers( index, itemstack, doReplaceRendering );
	}
	
	public void renderItemStackToAllPlayers( int index, ItemStack itemstack, boolean doReplaceRendering ) {
		for ( Player player : players ) {
			if ( player != null && player.isOnline() ) {
				renderItemStackToPlayer( player, index, itemstack, doReplaceRendering );
			}
		}
	}
	
	public void removeAllWithMetaIdFromAllPlayer( int metaid ) {
		removeAllWithMetaKeyFromAllPlayer( ActionItemManager.generateMetaKey( metaid ) );
	}
	
	public void removeAllWithMetaIdFromPlayer( Player player, int metaid ) {
		removeAllWithMetaKeyFromPlayer( player, ActionItemManager.generateMetaKey( metaid ) );
	}
	
	public void removeAllWithMetaKeyFromAllPlayer( String metakey ) {
		for ( Player player : players ) {
			if ( player != null && player.isOnline() ) {
				removeAllWithMetaKeyFromPlayer( player, metakey );
			}
		}
	}
	
	public void removeAllWithMetaKeyFromPlayer( Player player, String metakey ) {
		PlayerInventoryUtils.removeAllWithMetaKeyFromPlayerInventory( player.getInventory(), metakey );
	}
	
	public void removeItemStackFromPlayer( Player player, int index, ItemStack itemstack ) {
		PlayerInventoryUtils.removeItemStackFromPlayerInventory( player.getInventory(), index, itemstack );
	}
	
	public void removeAllItemStacksFromPlayer( Player player ) {
		for ( Entry<Integer, ItemStack> ise : itemstacks.entrySet() ) {
			removeItemStackFromPlayer( player, ise.getKey(), ise.getValue() );
		}
	}
	
	public void removeItemStackFromAllPlayers( int index, ItemStack itemstack ) {
		for ( Player player : players ) {
			if ( player != null && player.isOnline() ) {
				removeItemStackFromPlayer( player, index, itemstack );
			}
		}
	}
	
	// ******
	public void renderAllItemStacksToPlayerInventory( Inventory inv, boolean doReplaceRendering ) {
		for ( Entry<Integer, ItemStack> ise : itemstacks.entrySet() ) {
			PlayerInventoryUtils
					.renderItemStackToPlayerInventory( inv, ise.getKey(), ise.getValue(), doReplaceRendering );
		}
	}
	
	public void clearItemsWithMetaKey( Player player ) {
		for ( ActionItem ai : this.getActionItems() ) {
			ActionItemManager.removeItemsFromInventory( ai, player.getInventory() );
		}
	}
	
	@Override
	public void clearItemsWithMetaKey() {
		for ( Player player : players ) {
			if ( player != null && player.isOnline() ) {
				clearItemsWithMetaKey( player );
			}
		}
	}
	
	@Override
	public void openInventoryTo( Player player ) {
		if ( this.hasPlayer( player ) ) {
			player.openInventory( player.getInventory() );
		} else {
			Inventory temp = Bukkit.createInventory( null, InventoryType.PLAYER );
			renderAllItemStacksToPlayerInventory( temp, true );
			player.openInventory( temp );
		}
	}
	
	public RenderHandler getOnRenderHandler() {
		return onRenderHandler;
	}
	
	public void setOnRenderHandler( RenderHandler onRenderHandler ) {
		if ( onRenderHandler == null ) {
			this.onRenderHandler = DEFAULT_RENDERHANDLER;
		} else {
			this.onRenderHandler = onRenderHandler;
		}
	}
	
	@Override
	public int count( ActionItem actionItem ) {
		if ( !contains( actionItem ) ) {
			return 0;
		}
		String metakey = actionItem.getKey();
		int c = 0;
		for ( ItemStack is : this.itemstacks.values() ) {
			if ( metakey.equals( ActionItemManager.getMetaKey( is ) ) ) {
				c++;
			}
		}
		return c;
	}
	
}
