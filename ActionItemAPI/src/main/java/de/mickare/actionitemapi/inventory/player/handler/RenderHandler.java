package de.mickare.actionitemapi.inventory.player.handler;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.inventory.player.PlayerInventoryMenu;

public interface RenderHandler {
	
	public boolean onRender(PlayerInventoryMenu menu, Player player, int index, ItemStack itemstack, boolean doReplaceRendering);
	
}
