package de.mickare.actionitemapi.inventory;

import java.util.Set;

import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.EventListenerDeactivatable;
import de.mickare.actionitemapi.api.inventory.InventoryManager;
import de.mickare.actionitemapi.api.inventory.InventoryMenu;

public abstract class AbstractInventoryManager<M extends InventoryManager<M,I,A>, I extends InventoryMenu<M,I,A>, A extends ActionItem> implements EventListenerDeactivatable, InventoryManager<M,I,A> {
//public interface InventoryManager<I extends ActionItem, M extends InventoryMenu<I>> {

	private final JavaPlugin plugin;
	
	private boolean activatedListener = false;
	
	public AbstractInventoryManager(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	// *************************************
	
	public abstract void registerInventory( I menu );

	public abstract void unregisterInventory( I menu );
	
	public abstract Set<I> getMenues();
	
	// *************************************
	
	public JavaPlugin getPlugin() {
		return plugin;
	}
	
	@SuppressWarnings("unchecked")
	public M getSelf() {
		return (M) this;
	}
	
	public boolean isActivatedListener() {
		return activatedListener;
	}

	public void setActivatedListener( boolean activatedListener ) {
		this.activatedListener = activatedListener;
	}
	
}
