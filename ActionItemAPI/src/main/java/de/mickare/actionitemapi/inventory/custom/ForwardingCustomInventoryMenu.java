package de.mickare.actionitemapi.inventory.custom;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Multimap;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryManager;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryMenu;
import de.mickare.actionitemapi.api.inventory.actionitem.CustomInventoryActionItem;
import de.mickare.actionitemapi.events.PreClickEvent;
import de.mickare.actionitemapi.events.PreDropClickEvent;
import de.mickare.actionitemapi.events.PreDropEvent;
import de.mickare.actionitemapi.events.PreMoveEvent;
import de.mickare.actionitemapi.events.PrePickupEvent;
import de.mickare.actionitemapi.events.PreUseEvent;
import de.mickare.actionitemapi.inventory.AbstractForwardingInventoryMenu;

public class ForwardingCustomInventoryMenu extends
		AbstractForwardingInventoryMenu<ICustomInventoryManager, ICustomInventoryMenu, CustomInventoryActionItem>
		implements ICustomInventoryMenu {
	
	public ForwardingCustomInventoryMenu( ICustomInventoryMenu handle ) {
		super( handle );
	}
	
	@Override
	public CustomInventoryActionItem _newItem( int id ) {
		return getHandle()._newItem( id );
	}
	
	@Override
	public CustomInventoryActionItem newItem( int id ) {
		return getHandle().newItem( id );
	}
	
	@Override
	public CustomInventoryActionItem newItem( int id, boolean register ) {
		return getHandle().newItem( id, register );
	}
	
	@Override
	public CustomInventoryActionItem newItem( int id, ItemStack... itemstacks ) {
		return getHandle().newItem( id, itemstacks );
	}
	
	@Override
	public CustomInventoryActionItem newItem( int id, boolean register, ItemStack... itemstacks ) {
		return getHandle().newItem( id, register, itemstacks );
	}
	
	@Override
	public CustomInventoryActionItem setItem( int index, int id, ItemStack itemstack ) {
		return getHandle().setItem( index, id, itemstack );
	}
	
	@Override
	public CustomInventoryActionItem setItem( int index, CustomInventoryActionItem item, ItemStack itemstack ) {
		return getHandle().setItem( index, item, itemstack );
	}
	
	@Override
	public CustomInventoryActionItem[] getItems() {
		return getHandle().getItems();
	}
	
	@Override
	public ForwardingCustomInventoryMenu getSelf() {
		return this;
	}
	
	@Override
	public ICustomInventoryManager getManager() {
		return getHandle().getManager();
	}
	
	@Override
	public CustomInventoryActionItem getActionItem( int metaid ) {
		return getHandle().getActionItem( metaid );
	}
	
	@Override
	public CustomInventoryActionItem getActionItem( String metakey ) {
		return getHandle().getActionItem( metakey );
	}
	
	@Override
	public void addActionItem( CustomInventoryActionItem actionitem ) {
		getHandle().addActionItem( actionitem );
	}
	
	@Override
	public CustomInventoryActionItem removeActionItem( int metaid ) {
		return getHandle().removeActionItem( metaid );
	}
	
	@Override
	public CustomInventoryActionItem removeActionItem( String metakey ) {
		return getHandle().removeActionItem( metakey );
	}
	
	@Override
	public CustomInventoryActionItem removeActionItem( ActionItem actionitem ) {
		return getHandle().removeActionItem( actionitem );
	}
	
	@Override
	public Set<CustomInventoryActionItem> getActionItems() {
		return getHandle().getActionItems();
	}
	
	@Override
	public Inventory getInventory() {
		return getHandle().getInventory();
	}
	
	@Override
	public void callClickOnItem( PreClickEvent event, ActionItem item ) {
		getHandle().callClickOnItem( event, item );
	}
	
	@Override
	public void callUseOnItem( PreUseEvent event, ActionItem item ) {
		getHandle().callUseOnItem( event, item );
	}
	
	@Override
	public void callDropOnItem( PreDropEvent event, ActionItem item ) {
		getHandle().callDropOnItem( event, item );
	}
	
	@Override
	public void callDropOnItem( PreDropClickEvent event, ActionItem item ) {
		getHandle().callDropOnItem( event, item );
	}
	
	@Override
	public void callPickupOnItem( PrePickupEvent event, ActionItem item ) {
		getHandle().callPickupOnItem( event, item );
	}
	
	@Override
	public void callMoveOnItem( PreMoveEvent event, ActionItem item ) {
		getHandle().callMoveOnItem( event, item );
	}
	
	@Override
	public void registerHandlerAction( InventoryAction... action ) {
		getHandle().registerHandlerAction( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> getBlockedActions() {
		return getHandle().getBlockedActions();
	}
	
	@Override
	public Set<ClickType> getBlockedActions( InventoryAction action ) {
		return getHandle().getBlockedActions( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedAction() {
		return getHandle().clearBlockedAction();
	}
	
	@Override
	public Set<ClickType> clearBlockedAction( InventoryAction action ) {
		return getHandle().clearBlockedAction( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedActionAll( Set<InventoryAction> actions ) {
		return getHandle().clearBlockedActionAll( actions );
	}
	
	@Override
	public void blockAction( InventoryAction... action ) {
		getHandle().blockAction( action );
	}
	
	@Override
	public void blockAction( InventoryAction action, ClickType type, ClickType... types ) {
		getHandle().blockAction( action, type, types );
	}
	
	@Override
	public void blockActionAll( InventoryAction action, Collection<ClickType> types ) {
		getHandle().blockActionAll( action, types );
	}
	
	@Override
	public void blockActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		getHandle().blockActionAll( multimap );
	}
	
	@Override
	public void blockActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		getHandle().blockActionAll( multimap );
	}
	
	@Override
	public boolean hasBlockedAction( InventoryAction action, ClickType type ) {
		return getHandle().hasBlockedAction( action, type );
	}
}
