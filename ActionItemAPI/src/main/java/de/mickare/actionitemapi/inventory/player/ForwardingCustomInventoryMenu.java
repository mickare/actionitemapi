package de.mickare.actionitemapi.inventory.player;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Multimap;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryManager;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryMenu;
import de.mickare.actionitemapi.api.inventory.actionitem.PlayerInventoryActionItem;
import de.mickare.actionitemapi.events.PreClickEvent;
import de.mickare.actionitemapi.events.PreDropClickEvent;
import de.mickare.actionitemapi.events.PreDropEvent;
import de.mickare.actionitemapi.events.PreMoveEvent;
import de.mickare.actionitemapi.events.PrePickupEvent;
import de.mickare.actionitemapi.events.PreUseEvent;
import de.mickare.actionitemapi.inventory.AbstractForwardingInventoryMenu;
import de.mickare.actionitemapi.inventory.player.handler.RenderHandler;

public class ForwardingCustomInventoryMenu extends
		AbstractForwardingInventoryMenu<IPlayerInventoryManager, IPlayerInventoryMenu, PlayerInventoryActionItem>
		implements IPlayerInventoryMenu {
	
	public ForwardingCustomInventoryMenu( IPlayerInventoryMenu handle ) {
		super( handle );
	}
	
	@Override
	public PlayerInventoryActionItem _newItem( int id ) {
		return getHandle()._newItem( id );
	}
	
	@Override
	public PlayerInventoryActionItem newItem( int id ) {
		return getHandle().newItem( id );
	}
	
	@Override
	public PlayerInventoryActionItem newItem( int id, boolean register ) {
		return getHandle().newItem( id, register );
	}
	
	@Override
	public PlayerInventoryActionItem newItem( int id, ItemStack... itemstacks ) {
		return getHandle().newItem( id, itemstacks );
	}
	
	@Override
	public PlayerInventoryActionItem newItem( int id, boolean register, ItemStack... itemstacks ) {
		return getHandle().newItem( id, register, itemstacks );
	}
	
	@Override
	public PlayerInventoryActionItem setItem( int index, int id, ItemStack itemstack ) {
		return getHandle().setItem( index, id, itemstack );
	}
	
	@Override
	public PlayerInventoryActionItem setItem( int index, PlayerInventoryActionItem item, ItemStack itemstack ) {
		return getHandle().setItem( index, item, itemstack );
	}
	
	@Override
	public PlayerInventoryActionItem[] getItems() {
		return getHandle().getItems();
	}
	
	@Override
	public ForwardingCustomInventoryMenu getSelf() {
		return this;
	}
	
	@Override
	public IPlayerInventoryManager getManager() {
		return getHandle().getManager();
	}
	
	@Override
	public PlayerInventoryActionItem getActionItem( int metaid ) {
		return getHandle().getActionItem( metaid );
	}
	
	@Override
	public PlayerInventoryActionItem getActionItem( String metakey ) {
		return getHandle().getActionItem( metakey );
	}
	
	@Override
	public void addActionItem( PlayerInventoryActionItem actionitem ) {
		getHandle().addActionItem( actionitem );
	}
	
	@Override
	public PlayerInventoryActionItem removeActionItem( int metaid ) {
		return getHandle().removeActionItem( metaid );
	}
	
	@Override
	public PlayerInventoryActionItem removeActionItem( String metakey ) {
		return getHandle().removeActionItem( metakey );
	}
	
	@Override
	public PlayerInventoryActionItem removeActionItem( ActionItem actionitem ) {
		return getHandle().removeActionItem( actionitem );
	}
	
	@Override
	public Set<PlayerInventoryActionItem> getActionItems() {
		return getHandle().getActionItems();
	}
	
	@Override
	public String[] getPlayers() {
		return getHandle().getPlayers();
	}
	
	@Override
	public boolean isDoReplaceRendering() {
		return getHandle().isDoReplaceRendering();
	}
	
	@Override
	public void setDoReplaceRendering( boolean doReplaceRendering ) {
		getHandle().setDoReplaceRendering( doReplaceRendering );
	}
	
	@Override
	public void setItem( int index, ItemStack itemstack, boolean doReplaceRendering ) {
		getHandle().setItem( index, itemstack, doReplaceRendering );
	}
	
	@Override
	public boolean hasPlayer( Player player ) {
		return getHandle().hasPlayer( player );
	}
	
	@Override
	public boolean hasPlayer( UUID uuid ) {
		return getHandle().hasPlayer( uuid );
	}
	
	@Override
	public void addPlayer( Player player ) {
		getHandle().addPlayer( player );
	}
	
	@Override
	public void removePlayer( Player player ) {
		getHandle().removePlayer( player );
	}
	
	@Override
	public void renderItemStackToPlayer( Player player, int index, ItemStack itemstack ) {
		getHandle().renderItemStackToPlayer( player, index, itemstack );
	}
	
	@Override
	public void renderItemStackToPlayer( Player player, int index, ItemStack itemstack, boolean doReplaceRendering ) {
		getHandle().renderItemStackToPlayer( player, index, itemstack, doReplaceRendering );
	}
	
	@Override
	public void renderAllItemStacksToPlayer( Player player ) {
		getHandle().renderAllItemStacksToPlayer( player );
	}
	
	@Override
	public void renderAllItemStacksToPlayer( Player player, boolean doReplaceRendering ) {
		getHandle().renderAllItemStacksToPlayer( player, doReplaceRendering );
	}
	
	@Override
	public void renderItemStackToAllPlayers( int index, ItemStack itemstack ) {
		getHandle().renderItemStackToAllPlayers( index, itemstack );
	}
	
	@Override
	public void renderItemStackToAllPlayers( int index, ItemStack itemstack, boolean doReplaceRendering ) {
		getHandle().renderItemStackToAllPlayers( index, itemstack, doReplaceRendering );
	}
	
	@Override
	public void removeAllWithMetaIdFromAllPlayer( int metaid ) {
		getHandle().removeAllWithMetaIdFromAllPlayer( metaid );
	}
	
	@Override
	public void removeAllWithMetaIdFromPlayer( Player player, int metaid ) {
		getHandle().removeAllWithMetaIdFromPlayer( player, metaid );
	}
	
	@Override
	public void removeAllWithMetaKeyFromAllPlayer( String metakey ) {
		getHandle().removeAllWithMetaKeyFromAllPlayer( metakey );
	}
	
	@Override
	public void removeAllWithMetaKeyFromPlayer( Player player, String metakey ) {
		getHandle().removeAllWithMetaKeyFromPlayer( player, metakey );
	}
	
	@Override
	public void removeItemStackFromPlayer( Player player, int index, ItemStack itemstack ) {
		getHandle().removeItemStackFromPlayer( player, index, itemstack );
	}
	
	@Override
	public void removeAllItemStacksFromPlayer( Player player ) {
		getHandle().removeAllItemStacksFromPlayer( player );
	}
	
	@Override
	public void removeItemStackFromAllPlayers( int index, ItemStack itemstack ) {
		getHandle().removeItemStackFromAllPlayers( index, itemstack );
	}
	
	@Override
	public void renderAllItemStacksToPlayerInventory( Inventory inv, boolean doReplaceRendering ) {
		getHandle().renderAllItemStacksToPlayerInventory( inv, doReplaceRendering );
	}
	
	@Override
	public RenderHandler getOnRenderHandler() {
		return getHandle().getOnRenderHandler();
	}
	
	@Override
	public void setOnRenderHandler( RenderHandler onRenderHandler ) {
		getHandle().setOnRenderHandler( onRenderHandler );
	}
	
	@Override
	public void callClickOnItem( PreClickEvent event, ActionItem item ) {
		getHandle().callClickOnItem( event, item );
	}
	
	@Override
	public void callUseOnItem( PreUseEvent event, ActionItem item ) {
		getHandle().callUseOnItem( event, item );
	}

	@Override
	public void callDropOnItem( PreDropEvent event, ActionItem item ) {
		getHandle().callDropOnItem( event, item );
	}

	@Override
	public void callDropOnItem( PreDropClickEvent event, ActionItem item ) {
		getHandle().callDropOnItem( event, item );
	}
	
	@Override
	public void callPickupOnItem( PrePickupEvent event, ActionItem item ) {
		getHandle().callPickupOnItem( event, item );
	}
	
	@Override
	public void callMoveOnItem( PreMoveEvent event, ActionItem item ) {
		getHandle().callMoveOnItem( event, item );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> getBlockedActions() {
		return getHandle().getBlockedActions();
	}
	
	@Override
	public Set<ClickType> getBlockedActions( InventoryAction action ) {
		return getHandle().getBlockedActions( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedAction() {
		return getHandle().clearBlockedAction();
	}
	
	@Override
	public Set<ClickType> clearBlockedAction( InventoryAction action ) {
		return getHandle().clearBlockedAction( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedActionAll( Set<InventoryAction> actions ) {
		return getHandle().clearBlockedActionAll( actions );
	}
	
	@Override
	public void blockAction( InventoryAction... actions ) {
		getHandle().blockAction( actions );
	}
	
	@Override
	public void blockAction( InventoryAction action, ClickType type, ClickType... types ) {
		getHandle().blockAction( action, type, types );
	}
	
	@Override
	public void blockActionAll( InventoryAction action, Collection<ClickType> types ) {
		getHandle().blockActionAll( action, types );
	}
	
	@Override
	public void blockActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		getHandle().blockActionAll( multimap );
	}
	
	@Override
	public void blockActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		getHandle().blockActionAll( multimap );
	}
	
	@Override
	public boolean hasBlockedAction( InventoryAction action, ClickType type ) {
		return getHandle().hasBlockedAction( action, type );
	}
	
	@Override
	public void registerHandlerAction( InventoryAction... actions ) {
		getHandle().registerHandlerAction( actions );
	}
	
}
