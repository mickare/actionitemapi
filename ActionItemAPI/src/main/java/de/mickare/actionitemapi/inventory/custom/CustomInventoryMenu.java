package de.mickare.actionitemapi.inventory.custom;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.ActionItemManager;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryManager;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryMenu;
import de.mickare.actionitemapi.api.inventory.actionitem.CustomInventoryActionItem;
import de.mickare.actionitemapi.inventory.AbstractInventoryMenu;

public class CustomInventoryMenu extends
		AbstractInventoryMenu<ICustomInventoryManager, ICustomInventoryMenu, CustomInventoryActionItem> implements
		ICustomInventoryMenu {
	
	// private final int lines;
	
	private Inventory inventory;
	
	public CustomInventoryMenu( CustomInventoryManager manager, JavaPlugin plugin, Inventory inventory ) {
		super( manager, plugin );
		// this.lines = lines;
		// Validate.isTrue( lines >= 0 && lines <= 6,
		// "lines can only be from 0 to 6" );
		this.inventory = inventory;
	}
	
	public Inventory getInventory() {
		return inventory;
	}
	
	@Override
	public CustomInventoryActionItem _newItem( int id ) {
		return new CustomInventoryActionItem( id, this );
	}
	
	@Override
	public CustomInventoryActionItem setItem( int index, int id, ItemStack itemstack ) {
		CustomInventoryActionItem item = this.getActionItem( id );
		if ( item == null ) {
			item = _newItem( id );
		}
		setItem( index, item, itemstack );
		return item;
	}
	
	@Override
	public CustomInventoryActionItem setItem( int index, CustomInventoryActionItem actionitem, ItemStack itemstack ) {
		actionitem.addMetaKey( itemstack );
		inventory.setItem( index, itemstack );
		if ( !this.contains( actionitem ) ) {
			this.addActionItem( actionitem );
			ActionItemAPI.getActionItemManager().registerActionItem( actionitem );
		}
		return actionitem;
	}
	
	@Override
	public void setItem( int index, ItemStack itemstack ) {
		inventory.setItem( index, itemstack );
	}
	
	@Override
	public void removeItem( ActionItem item ) {
		removeItem( item.getKey() );
	}
	
	@Override
	public void removeItem( String metakey ) {
		this.removeItem( metakey );
		ActionItemManager.removeItemsFromInventory( metakey, inventory );
	}
	
	@Override
	public void removeItem( int metaid ) {
		removeItem( ActionItemManager.generateMetaKey( metaid ) );
	}
	
	@Override
	public void clear() {
		this.clearActionItems();
		inventory.clear();
	}
	
	@Override
	public void clear( int index ) {
		ItemStack old = inventory.getItem( index );
		inventory.clear( index );
		if ( old != null ) {
			String metakey = ActionItemManager.getMetaKey( old );
			if ( metakey != null ) {
				
				boolean last = true;
				
				for ( ItemStack is : inventory ) {
					if ( metakey.equals( ActionItemManager.getMetaKey( is ) ) ) {
						last = false;
						break;
					}
				}
				
				if ( last ) {
					this.removeActionItem( metakey );
				}
			}
		}
		
	}
	
	@Override
	public boolean contains( String itemkey ) {
		return this.hasActionItem( itemkey );
	}
	
	@Override
	public boolean contains( ActionItem item ) {
		return this.hasActionItem( item );
	}
	
	@Override
	public CustomInventoryActionItem[] getItems() {
		return this.getActionItems().toArray( new CustomInventoryActionItem[0] );
	}
	
	@Override
	public void clearItemsWithMetaKey() {
		for ( ActionItem ai : this.getActionItems() ) {
			ActionItemManager.removeItemsFromInventory( ai, inventory );
		}
	}
	
	@Override
	public void openInventoryTo( Player player ) {
		player.openInventory( this.getInventory() );
	}
	
	@Override
	public int count( ActionItem actionItem ) {
		if ( !contains( actionItem ) ) {
			return 0;
		}
		String metakey = actionItem.getKey();
		int c = 0;
		for ( ItemStack is : inventory ) {
			if ( metakey.equals( ActionItemManager.getMetaKey( is ) ) ) {
				c++;
			}
		}
		return c;
	}
	
	
}
