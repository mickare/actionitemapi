package de.mickare.actionitemapi.inventory;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Multimap;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.inventory.InventoryManager;
import de.mickare.actionitemapi.api.inventory.InventoryMenu;

public abstract class AbstractForwardingInventoryMenu<M extends InventoryManager<M,I,A>, I extends InventoryMenu<M,I,A>, A extends ActionItem> implements InventoryMenu<M, I, A> {

	private final I handle;

	public AbstractForwardingInventoryMenu(I handle) {
		this.handle = handle;
	}

	public I getHandle() {
		return handle;
	}

	@Override
	public void setItem(int index, ItemStack itemstack) {
		handle.setItem(index, itemstack);
	}

	@Override
	public void removeItem(ActionItem item) {
		handle.removeItem(item);
	}

	@Override
	public void removeItem(String metakey) {
		handle.removeItem(metakey);
	}

	@Override
	public void removeItem(int metaid) {
		handle.removeItem(metaid);
	}

	@Override
	public boolean contains(String itemkey) {
		return handle.contains(itemkey);
	}

	@Override
	public boolean contains(ActionItem item) {
		return handle.contains(item);
	}

	@Override
	public int count(ActionItem actionItem) {
		return handle.count(actionItem);
	}

	@Override
	public void clear() {
		handle.clear();
	}

	@Override
	public void clear(int index) {
		handle.clear(index);
	}

	@Override
	public JavaPlugin getPlugin() {
		return handle.getPlugin();
	}

	@Override
	public void clearActionItems() {
		handle.clearActionItems();
	}

	@Override
	public boolean hasActionItem(int metaid) {
		return handle.hasActionItem(metaid);
	}

	@Override
	public boolean hasActionItem(String metakey) {
		return handle.hasActionItem(metakey);
	}

	@Override
	public boolean hasActionItem(ActionItem actionitem) {
		return handle.hasActionItem(actionitem);
	}

	@Override
	public void clearItemsWithMetaKey() {
		handle.clearItemsWithMetaKey();
	}

	@Override
	public void clearItemsWithMetaKey(int metaid_start, int metaid_end) {
		handle.clearItemsWithMetaKey(metaid_start, metaid_end);
	}

	@Override
	public void openInventoryTo(Player player) {
		handle.openInventoryTo(player);
	}
	
	// ****************************************************************
	// Allowed Actions
	@Override
	public Multimap<InventoryAction, ClickType> getRegisteredHandlerActions() {
		return handle.getRegisteredHandlerActions();
	}
	
	@Override
	public Set<ClickType> getRegisteredHandlerActions( InventoryAction action ) {
		return handle.getRegisteredHandlerActions( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearRegisteredHandlerAction() {
		return handle.clearRegisteredHandlerAction();
	}
	
	@Override
	public Set<ClickType> clearRegisteredHandlerAction( InventoryAction action ) {
		return handle.clearRegisteredHandlerAction( action );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearRegisteredHandlerActionAll( Set<InventoryAction> actions ) {
		return handle.clearRegisteredHandlerActionAll( actions );
	}
	
	@Override
	public void registerHandlerAction( InventoryAction action, ClickType type, ClickType... types ) {
		handle.registerHandlerAction( action, type, types );
	}
	
	@Override
	public void registerHandlerActionAll( InventoryAction action, Collection<ClickType> types ) {
		handle.registerHandlerActionAll( action, types );
	}
	
	@Override
	public void registerHandlerActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		handle.registerHandlerActionAll( multimap );
	}
	
	@Override
	public void registerHandlerActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		handle.registerHandlerActionAll( multimap );
	}
	
	@Override
	public boolean hasRegisteredHandlerAction( InventoryAction action, ClickType type ) {
		return handle.hasRegisteredHandlerAction( action, type );
	}
	
}
