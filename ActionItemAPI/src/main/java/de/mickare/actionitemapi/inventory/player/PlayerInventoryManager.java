package de.mickare.actionitemapi.inventory.player;

import static de.mickare.actionitemapi.ActionItemAPI.logInfo;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryManager;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryMenu;
import de.mickare.actionitemapi.api.inventory.actionitem.PlayerInventoryActionItem;
import de.mickare.actionitemapi.inventory.AbstractInventoryManager;

public class PlayerInventoryManager extends
		AbstractInventoryManager<IPlayerInventoryManager, IPlayerInventoryMenu, PlayerInventoryActionItem> implements
		Listener, IPlayerInventoryManager {
	
	public final HashSet<IPlayerInventoryMenu> menues = new HashSet<IPlayerInventoryMenu>();
	
	public PlayerInventoryManager( JavaPlugin plugin ) {
		super( plugin );
	}
	
	@Override
	public void registerInventory( IPlayerInventoryMenu menu ) {
		menues.add( menu );
	}
	
	@Override
	public void unregisterInventory( IPlayerInventoryMenu menu ) {
		menues.remove( menu );
		for ( String playername : menu.getPlayers() ) {
			Player player = Bukkit.getPlayer( playername );
			if ( player != null ) {
				menu.removeAllItemStacksFromPlayer( player );
			}
		}
	}
	
	public void removePlayerFromAll( Player player ) {
		for ( IPlayerInventoryMenu menu : menues ) {
			menu.removeAllItemStacksFromPlayer( player );
		}
	}
	
	@Override
	public Set<IPlayerInventoryMenu> getMenues() {
		return menues;
	}
	
	@Override
	public IPlayerInventoryMenu createMenu( boolean doReplaceRendering ) {
		PlayerInventoryMenu menu = new PlayerInventoryMenu( getSelf(), getPlugin(), doReplaceRendering );
		this.registerInventory( menu );
		return menu;
	}
	
	@Override
	public PlayerInventoryManager getSelf() {
		return this;
	}
	
	/*
	 * @EventHandler( priority = EventPriority.MONITOR, ignoreCancelled = false ) public void onInventoryTEST(
	 * InventoryClickEvent event ) { logInfo( ">> InventoryClickEvent Cancelled: ", event.isCancelled() ); }
	 */
	
	@EventHandler( priority = EventPriority.NORMAL, ignoreCancelled = false )
	public void onInventoryClickAllowed( InventoryClickEvent event ) {
		checkEvent( event, true );
	}
	
	@EventHandler( priority = EventPriority.HIGHEST, ignoreCancelled = false )
	public void onInventoryClickBlocked( InventoryClickEvent event ) {
		checkEvent( event, false );
	}
	
	private void checkEvent( InventoryClickEvent event, boolean allowedAction ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		
		logInfo( "+--------------------------" );
		logInfo( "> InventoryClickEvent" );
		logInfo( "| PlayerInventory" );
		
		final InventoryAction action = event.getAction();
		final ClickType click = event.getClick();
		
		final HumanEntity human = event.getWhoClicked();
		logInfo( "| Human clicked: ", human.getName() );
		
		if ( !( human instanceof Player ) ) {
			logInfo( "| No Player!" );
			logInfo( "<" );
			return;
		}
		
		final Player player = ( Player ) human;
		boolean didCancelled = false;
		
		if ( allowedAction ) {
			if ( event.getView().getBottomInventory().equals( player.getInventory() ) ) {
				for ( final IPlayerInventoryMenu m : menues ) {
					if ( m.hasPlayer( player ) ) {
						logInfo( "| Menu found that has Player!" );
						if ( !m.hasRegisteredHandlerAction( action, click ) ) {
							ActionItemAPI.logInfo( "| Action not allowed!" );
							didCancelled = true;
							event.setCancelled( true );
						}
						break;
					}
				}
			}
		} else {
			if ( event.getView().getBottomInventory().equals( player.getInventory() ) ) {
				for ( final IPlayerInventoryMenu m : menues ) {
					if ( m.hasPlayer( player ) ) {
						logInfo( "| Menu found that has Player!" );
						if ( m.hasBlockedAction( action, click ) ) {
							ActionItemAPI.logInfo( "| Action blocked!" );
							didCancelled = true;
							event.setCancelled( true );
						}
						break;
					}
				}
			}
		}
		
		if ( didCancelled ) {
			Bukkit.getScheduler().runTask( ActionItemAPI.getPlugin(), new Runnable() {
				@Override
				public void run() {
					player.updateInventory();
				}
			} );
		}
		logInfo( "<" );
	}
	
}
