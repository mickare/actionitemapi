package de.mickare.actionitemapi.inventory.custom;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.ActionItemAPI;
import static de.mickare.actionitemapi.ActionItemAPI.logInfo;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryManager;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryMenu;
import de.mickare.actionitemapi.api.inventory.actionitem.CustomInventoryActionItem;
import de.mickare.actionitemapi.inventory.AbstractInventoryManager;

public class CustomInventoryManager extends
		AbstractInventoryManager<ICustomInventoryManager, ICustomInventoryMenu, CustomInventoryActionItem> implements
		Listener, ICustomInventoryManager {
	
	public static final int MAX_LINES = 6;
	
	public final HashSet<ICustomInventoryMenu> menues = new HashSet<ICustomInventoryMenu>();
	
	public CustomInventoryManager( JavaPlugin plugin ) {
		super( plugin );
	}
	
	public ICustomInventoryMenu createMenu( JavaPlugin plugin, InventoryHolder owner, int lines, String title ) {
		Validate.isTrue( lines >= 0 && lines <= MAX_LINES, "lines can only be from 0 to 6" );
		return createMenu( plugin, Bukkit.createInventory( owner, lines * 9, title ) );
	}
	
	public ICustomInventoryMenu createMenu( JavaPlugin plugin, Inventory inventory ) {
		CustomInventoryMenu menu = new CustomInventoryMenu( this, plugin, inventory );
		registerInventory( menu );
		return menu;
	}
	
	@Override
	public void registerInventory( ICustomInventoryMenu menu ) {
		menues.add( menu );
	}
	
	@Override
	public void unregisterInventory( ICustomInventoryMenu menu ) {
		for ( HumanEntity he : menu.getInventory().getViewers().toArray( new HumanEntity[0] ) ) {
			he.closeInventory();
		}
		menues.remove( menu );
	}
	
	@Override
	public Set<ICustomInventoryMenu> getMenues() {
		return menues;
	}
	
	@EventHandler( priority = EventPriority.NORMAL, ignoreCancelled = false )
	public void onInventoryClickAllowed( InventoryClickEvent event ) {
		checkEvent( event, true );
	}
	
	@EventHandler( priority = EventPriority.HIGHEST, ignoreCancelled = false )
	public void onInventoryClickBlocked( InventoryClickEvent event ) {
		checkEvent( event, false );
	}
	
	private void checkEvent( InventoryClickEvent event, boolean allowedAction ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		
		logInfo( "+--------------------------" );
		logInfo( "> InventoryClickEvent" );
		logInfo( "| CustomInventory" );
		
		final HumanEntity human = event.getWhoClicked();
		logInfo( "| Human clicked: ", human.getName() );
		
		if ( !( human instanceof Player ) ) {
			logInfo( "| No Player!" );
			logInfo( "<" );
			return;
		}
		final Player player = ( Player ) human;
		
		final InventoryAction action = event.getAction();
		final ClickType click = event.getClick();
		
		final Inventory top = event.getView().getTopInventory();
		final Inventory bottom = event.getView().getBottomInventory();
		
		boolean didCancelled = false;
		
		if ( allowedAction ) {
			// Top
			for ( ICustomInventoryMenu m : this.menues ) {
				if ( m.getInventory().equals( top ) ) {
					logInfo( "| Menu found that has top inventory!" );
					if ( !m.hasRegisteredHandlerAction( action, click ) ) {
						logInfo( "| Action not allowed!" );
						didCancelled = true;
						event.setCancelled( true );
					}
					break;
				}
			}
			for ( ICustomInventoryMenu m : this.menues ) {
				if ( m.getInventory().equals( bottom ) ) {
					logInfo( "| Menu found that has bottom inventory!" );
					if ( !m.hasRegisteredHandlerAction( action, click ) ) {
						logInfo( "| Action not allowed!" );
						didCancelled = true;
						event.setCancelled( true );
					}
					break;
				}
			}
		} else {
			// Top
			for ( ICustomInventoryMenu m : this.menues ) {
				if ( m.getInventory().equals( top ) ) {
					logInfo( "| Menu found that has top inventory!" );
					if ( m.hasBlockedAction( action, click ) ) {
						logInfo( "| Action blocked!" );
						didCancelled = true;
						event.setCancelled( true );
					}
					break;
				}
			}
			for ( ICustomInventoryMenu m : this.menues ) {
				if ( m.getInventory().equals( bottom ) ) {
					logInfo( "| Menu found that has bottom inventory!" );
					if ( m.hasBlockedAction( action, click ) ) {
						logInfo( "| Action blocked!" );
						didCancelled = true;
						event.setCancelled( true );
					}
					break;
				}
			}
		}
		
		if ( didCancelled ) {
			Bukkit.getScheduler().runTask( ActionItemAPI.getPlugin(), new Runnable() {
				@Override
				public void run() {
					player.updateInventory();
					// event.getView().
					// player.openInventory( top );
				}
			} );
		}
		
		logInfo( "<" );
	}
}
