package de.mickare.actionitemapi.inventory;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.ActionItemManager;
import de.mickare.actionitemapi.api.inventory.InventoryManager;
import de.mickare.actionitemapi.api.inventory.InventoryMenu;
import de.mickare.actionitemapi.events.PreClickEvent;
import de.mickare.actionitemapi.events.PreDropClickEvent;
import de.mickare.actionitemapi.events.PreDropEvent;
import de.mickare.actionitemapi.events.PreMoveEvent;
import de.mickare.actionitemapi.events.PrePickupEvent;
import de.mickare.actionitemapi.events.PreUseEvent;

public abstract class AbstractInventoryMenu<M extends InventoryManager<M, I, A>, I extends InventoryMenu<M, I, A>, A extends ActionItem>
		implements InventoryMenu<M, I, A> {
	
	private final M manager;
	private final JavaPlugin plugin;
	
	private final HashMap<String, A> actionitems = new HashMap<String, A>();
	private final HashMultimap<InventoryAction, ClickType> allowedActions = HashMultimap.create();
	private final HashMultimap<InventoryAction, ClickType> blockedActions = HashMultimap.create();
	
	public AbstractInventoryMenu( M manager, JavaPlugin plugin ) {
		this.manager = manager;
		this.plugin = plugin;
		
	}
	
	@Override
	public abstract A _newItem( int id );
	
	@Override
	public A newItem( int id ) {
		return newItem( id, true );
	}
	
	@Override
	public A newItem( int id, boolean register ) {
		A item = _newItem( id );
		if ( register ) {
			ActionItemAPI.getActionItemManager().registerActionItem( item );
		}
		return item;
	}
	
	@Override
	public A newItem( int id, ItemStack... itemstacks ) {
		return newItem( id, true, itemstacks );
	}
	
	@Override
	public A newItem( int id, boolean register, ItemStack... itemstacks ) {
		A item = newItem( id, register );
		for ( ItemStack is : itemstacks ) {
			item.addMetaKey( is );
		}
		return item;
	}
	
	@Override
	public abstract A setItem( int index, int id, ItemStack itemstack );
	
	@Override
	public abstract A setItem( int index, A item, ItemStack itemstack );
	
	@Override
	public abstract void setItem( int index, ItemStack itemstack );
	
	@Override
	public abstract void removeItem( ActionItem item );
	
	@Override
	public abstract void removeItem( String metakey );
	
	@Override
	public abstract void removeItem( int metaid );
	
	@Override
	public abstract boolean contains( String itemkey );
	
	@Override
	public abstract boolean contains( ActionItem item );
	
	@Override
	public abstract int count( ActionItem actionItem );
	
	@Override
	public abstract A[] getItems();
	
	@Override
	public abstract void clear();
	
	@Override
	public abstract void clear( int index );
	
	@Override
	@SuppressWarnings( "unchecked" )
	public I getSelf() {
		return ( I ) this;
	}
	
	@Override
	public JavaPlugin getPlugin() {
		return plugin;
	}
	
	@Override
	public M getManager() {
		return manager;
	}
	
	@Override
	public A getActionItem( int metaid ) {
		return getActionItem( ActionItemManager.generateMetaKey( metaid ) );
	}
	
	@Override
	public A getActionItem( String metakey ) {
		return actionitems.get( metakey );
	}
	
	@Override
	public void addActionItem( A actionitem ) {
		actionitems.put( actionitem.getKey(), actionitem );
	}
	
	@Override
	public A removeActionItem( int metaid ) {
		return removeActionItem( ActionItemManager.generateMetaKey( metaid ) );
	}
	
	@Override
	public A removeActionItem( String metakey ) {
		return actionitems.remove( metakey );
	}
	
	@Override
	public A removeActionItem( ActionItem actionitem ) {
		return removeActionItem( actionitem.getKey() );
	}
	
	@Override
	public void clearActionItems() {
		actionitems.clear();
	}
	
	@Override
	public Set<A> getActionItems() {
		return new HashSet<A>( actionitems.values() );
	}
	
	@Override
	public boolean hasActionItem( int metaid ) {
		return hasActionItem( ActionItemManager.generateMetaKey( metaid ) );
	}
	
	@Override
	public boolean hasActionItem( String metakey ) {
		return actionitems.containsKey( metakey );
	}
	
	public boolean hasActionItem( ActionItem actionitem ) {
		return actionitems.containsValue( actionitem );
	}
	
	@Override
	public abstract void clearItemsWithMetaKey();
	
	@Override
	public void clearItemsWithMetaKey( int metaid_start, int metaid_end ) {
		Preconditions.checkArgument( metaid_start <= metaid_end, "start has to be smaller/equal end!" );
		for ( int i = metaid_start; i < metaid_end; i++ ) {
			this.removeItem( i );
		}
	}
	
	@Override
	public abstract void openInventoryTo( Player player );
	
	// ****************************************************************
	// Allowed Actions
	@Override
	public Multimap<InventoryAction, ClickType> getRegisteredHandlerActions() {
		return ImmutableMultimap.copyOf( this.allowedActions );
	}
	
	@Override
	public Set<ClickType> getRegisteredHandlerActions( InventoryAction action ) {
		return ImmutableSet.copyOf( this.allowedActions.get( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearRegisteredHandlerAction() {
		Multimap<InventoryAction, ClickType> old = getRegisteredHandlerActions();
		this.allowedActions.clear();
		return old;
	}
	
	@Override
	public Set<ClickType> clearRegisteredHandlerAction( InventoryAction action ) {
		return ImmutableSet.copyOf( this.allowedActions.removeAll( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearRegisteredHandlerActionAll( Set<InventoryAction> actions ) {
		Multimap<InventoryAction, ClickType> result = HashMultimap.create();
		for ( InventoryAction action : actions ) {
			result.putAll( action, this.clearRegisteredHandlerAction( action ) );
		}
		return result;
	}
	
	@Override
	public void registerHandlerAction( InventoryAction... actions ) {
		for ( InventoryAction action : actions ) {
			this.allowedActions.putAll( action, Arrays.asList( ClickType.values() ) );
		}
	}
	
	@Override
	public void registerHandlerAction( InventoryAction action, ClickType type, ClickType... types ) {
		this.allowedActions.put( action, type );
		this.allowedActions.putAll( action, Arrays.asList( types ) );
	}
	
	@Override
	public void registerHandlerActionAll( InventoryAction action, Collection<ClickType> types ) {
		this.allowedActions.putAll( action, types );
	}
	
	@Override
	public void registerHandlerActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		this.registerHandlerActionAll( multimap.asMap() );
	}
	
	@Override
	public void registerHandlerActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		for ( Entry<InventoryAction, Collection<ClickType>> e : multimap.entrySet() ) {
			this.registerHandlerActionAll( e.getKey(), e.getValue() );
		}
	}
	
	@Override
	public boolean hasRegisteredHandlerAction( InventoryAction action, ClickType type ) {
		return this.allowedActions.containsEntry( action, type );
	}
	
	// ****************************************************************
	// Blocked Actions
	@Override
	public Multimap<InventoryAction, ClickType> getBlockedActions() {
		return ImmutableMultimap.copyOf( this.blockedActions );
	}
	
	@Override
	public Set<ClickType> getBlockedActions( InventoryAction action ) {
		return ImmutableSet.copyOf( this.blockedActions.get( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedAction() {
		Multimap<InventoryAction, ClickType> old = getBlockedActions();
		this.blockedActions.clear();
		return old;
	}
	
	@Override
	public Set<ClickType> clearBlockedAction( InventoryAction action ) {
		return ImmutableSet.copyOf( this.blockedActions.removeAll( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedActionAll( Set<InventoryAction> actions ) {
		Multimap<InventoryAction, ClickType> result = HashMultimap.create();
		for ( InventoryAction action : actions ) {
			result.putAll( action, this.clearBlockedAction( action ) );
		}
		return result;
	}
	
	@Override
	public void blockAction( InventoryAction... actions ) {
		for ( InventoryAction action : actions ) {
			this.blockedActions.putAll( action, Arrays.asList( ClickType.values() ) );
		}
	}
	
	@Override
	public void blockAction( InventoryAction action, ClickType type, ClickType... types ) {
		this.blockedActions.put( action, type );
		this.blockedActions.putAll( action, Arrays.asList( types ) );
	}
	
	@Override
	public void blockActionAll( InventoryAction action, Collection<ClickType> types ) {
		this.blockedActions.putAll( action, types );
	}
	
	@Override
	public void blockActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		this.blockActionAll( multimap.asMap() );
	}
	
	@Override
	public void blockActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		for ( Entry<InventoryAction, Collection<ClickType>> e : multimap.entrySet() ) {
			this.blockActionAll( e.getKey(), e.getValue() );
		}
	}
	
	@Override
	public boolean hasBlockedAction( InventoryAction action, ClickType type ) {
		return this.blockedActions.containsEntry( action, type );
	}
	
	// ****************************************************************
	// Calls by Items
	
	@Override
	public void callClickOnItem( PreClickEvent event, ActionItem item ) {
		if ( !hasRegisteredHandlerAction( event.getEvent().getAction(), event.getEvent().getClick() ) ) {
			ActionItemAPI.logInfo( "| Inventory denied Action" );
			event.setCancelled( true );
			return;
		}
		item.onClickPickup( event, this );
		if ( this.hasBlockedAction( event.getEvent().getAction(), event.getEvent().getClick() ) ) {
			ActionItemAPI.logInfo( "| Inventory blocked Action" );
			event.setCancelled( true );
			return;
		}
	}
	
	@Override
	public void callUseOnItem( PreUseEvent event, ActionItem item ) {
		item.onUse( event, this );
	}
	
	@Override
	public void callDropOnItem( PreDropEvent event, ActionItem item ) {
		item.onDrop( event, this );
	}
	
	@Override
	public void callDropOnItem( PreDropClickEvent event, ActionItem item ) {
		if ( !hasRegisteredHandlerAction( event.getEvent().getAction(), event.getEvent().getClick() ) ) {
			ActionItemAPI.logInfo( "| Inventory denied Action" );
			event.setCancelled( true );
			return;
		}
		item.onDrop( event, this );
		if ( this.hasBlockedAction( event.getEvent().getAction(), event.getEvent().getClick() ) ) {
			ActionItemAPI.logInfo( "| Inventory blocked Action" );
			event.setCancelled( true );
			return;
		}
	}
	
	@Override
	public void callPickupOnItem( PrePickupEvent event, ActionItem item ) {
		item.onPickup( event, this );
	}
	
	@Override
	public void callMoveOnItem( PreMoveEvent event, ActionItem item ) {
		if ( !hasRegisteredHandlerAction( event.getEvent().getAction(), event.getEvent().getClick() ) ) {
			ActionItemAPI.logInfo( "| Inventory denied Action" );
			event.setCancelled( true );
			return;
		}
		item.onMove( event, this );
		if ( this.hasBlockedAction( event.getEvent().getAction(), event.getEvent().getClick() ) ) {
			ActionItemAPI.logInfo( "| Inventory blocked Action" );
			event.setCancelled( true );
			return;
		}
	}
	
}
