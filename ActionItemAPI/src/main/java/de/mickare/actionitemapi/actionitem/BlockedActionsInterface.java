package de.mickare.actionitemapi.actionitem;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;

import com.google.common.collect.Multimap;

public interface BlockedActionsInterface {
	
	Multimap<InventoryAction, ClickType> getBlockedActions();
	
	Set<ClickType> getBlockedActions( InventoryAction action );
	
	Multimap<InventoryAction, ClickType> clearBlockedAction();
	
	Set<ClickType> clearBlockedAction( InventoryAction action );
	
	Multimap<InventoryAction, ClickType> clearBlockedActionAll( Set<InventoryAction> actions );
	
	void blockAction( InventoryAction... actions );
	
	void blockAction( InventoryAction action, ClickType type, ClickType... types );
	
	void blockActionAll( InventoryAction action, Collection<ClickType> types );
	
	void blockActionAll( Multimap<InventoryAction, ClickType> multimap );
	
	void blockActionAll( Map<InventoryAction, Collection<ClickType>> multimap );
	
	boolean hasBlockedAction( InventoryAction action, ClickType type );
	
}
