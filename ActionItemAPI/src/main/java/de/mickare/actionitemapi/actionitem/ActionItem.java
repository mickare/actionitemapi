package de.mickare.actionitemapi.actionitem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.handler.*;
import de.mickare.actionitemapi.api.inventory.InventoryMenu;
import de.mickare.actionitemapi.events.*;

public class ActionItem implements RegisterHandlerActionsInterface, BlockedActionsInterface {
	
	private static class NO_HANDLER implements PostActionItemHandler, PreActionItemHandler {
		@Override
		public void onPostClick( HandlerData data, PostClickEvent event ) {
		}
		
		@Override
		public void onPostPickup( HandlerData data, PostPickupEvent event ) {
		}
		
		@Override
		public void onPostUse( HandlerData data, PostUseEvent event ) {
		}
		
		@Override
		public void onPostMove( HandlerData data, PostMoveEvent event ) {
		}
		
		@Override
		public void onPostDrop( HandlerData data, PostDropEvent event ) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onPostDrop( HandlerData data, PostDropClickEvent event ) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onPreClick( HandlerData data, PreClickEvent event ) {
			ActionItemAPI.logInfo( "| No handler!" );
		}
		
		@Override
		public void onPrePickup( HandlerData data, PrePickupEvent event ) {
			ActionItemAPI.logInfo( "| No handler!" );
		}
		
		@Override
		public void onPreUse( HandlerData data, PreUseEvent event ) {
			ActionItemAPI.logInfo( "| No handler!" );
		}
		
		@Override
		public void onPreMove( HandlerData data, PreMoveEvent event ) {
			ActionItemAPI.logInfo( "| No handler!" );
		}
		
		@Override
		public void onPreDrop( HandlerData data, PreDropEvent event ) {
			ActionItemAPI.logInfo( "| No handler!" );
		}
		
		@Override
		public void onPreDrop( HandlerData data, PreDropClickEvent event ) {
			ActionItemAPI.logInfo( "| No handler!" );
		}
	};
	
	public final static ItemHandler DEFAULT_HANDLER = new NO_HANDLER();
	
	// ********************************************************************************************
	// Actions
	
	public static final class ACTIONS {
		private ACTIONS() {
		}
		
		// ********************************************************************************************
		// Actions that start a movement
		public static final Multimap<InventoryAction, ClickType> MOVE_START;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			b.put( InventoryAction.PICKUP_ALL, ClickType.LEFT );
			b.put( InventoryAction.PICKUP_HALF, ClickType.RIGHT );
			b.put( InventoryAction.CLONE_STACK, ClickType.MIDDLE );
			MOVE_START = b.build();
		}
		
		public static boolean isMoveStart( InventoryAction action, ClickType type ) {
			return MOVE_START.containsEntry( action, type );
		}
		
		// ********************************************************************************************
		// Actions that end a movement
		
		public static final Multimap<InventoryAction, ClickType> MOVE_END;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			b.put( InventoryAction.PLACE_ALL, ClickType.LEFT );
			b.put( InventoryAction.PLACE_SOME, ClickType.LEFT );
			b.put( InventoryAction.PLACE_ONE, ClickType.RIGHT );
			MOVE_END = b.build();
		}
		
		public static boolean isMoveEnd( InventoryAction action, ClickType type ) {
			return MOVE_END.containsEntry( action, type );
		}
		
		// ********************************************************************************************
		// Actions that move directly to another inventory
		
		public static final Multimap<InventoryAction, ClickType> MOVE_DIRECTLY;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			b.putAll( InventoryAction.MOVE_TO_OTHER_INVENTORY, ClickType.SHIFT_LEFT, ClickType.SHIFT_RIGHT );
			MOVE_DIRECTLY = b.build();
		}
		
		public static boolean isMoveDirectly( InventoryAction action, ClickType type ) {
			return MOVE_DIRECTLY.containsEntry( action, type );
		}
		
		// ********************************************************************************************
		// Actions that swap directly to hotbar
		
		public static final Multimap<InventoryAction, ClickType> HOTBAR_SWAP;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			b.put( InventoryAction.HOTBAR_SWAP, ClickType.NUMBER_KEY );
			b.put( InventoryAction.HOTBAR_MOVE_AND_READD, ClickType.NUMBER_KEY );
			HOTBAR_SWAP = b.build();
		}
		
		public static boolean isHotBarSwap( InventoryAction action, ClickType type ) {
			return HOTBAR_SWAP.containsEntry( action, type );
		}
		
		// ********************************************************************************************
		// Click Pickup
		public static final Multimap<InventoryAction, ClickType> CLICK;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			b.putAll( InventoryAction.PICKUP_ALL, ClickType.values() );
			b.putAll( InventoryAction.PICKUP_HALF, ClickType.values() );
			b.putAll( InventoryAction.PICKUP_ONE, ClickType.values() );
			b.putAll( InventoryAction.PICKUP_SOME, ClickType.values() );
			CLICK = b.build();
		}
		
		// ********************************************************************************************
		// Place
		public static final Multimap<InventoryAction, ClickType> PLACE;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			b.putAll( InventoryAction.PLACE_ALL, ClickType.values() );
			b.putAll( InventoryAction.PLACE_ONE, ClickType.values() );
			b.putAll( InventoryAction.PLACE_SOME, ClickType.values() );
			PLACE = b.build();
		}
		
		public static boolean isHotClickPickup( InventoryAction action, ClickType type ) {
			return HOTBAR_SWAP.containsEntry( action, type );
		}
		
		// ********************************************************************************************
		// All Actions
		public static final Multimap<InventoryAction, ClickType> ALL;
		static {
			ImmutableMultimap.Builder<InventoryAction, ClickType> b = ImmutableMultimap.builder();
			for ( InventoryAction action : InventoryAction.values() ) {
				b.putAll( action, ClickType.values() );
			}
			ALL = b.build();
		}
		
		public static final Multimap<InventoryAction, ClickType> getAllExcept(
				Collection<Multimap<InventoryAction, ClickType>> excepts ) {
			Multimap<InventoryAction, ClickType> result = HashMultimap.create( ALL );
			for ( Multimap<InventoryAction, ClickType> ex : excepts ) {
				for ( Entry<InventoryAction, ClickType> e : ex.entries() ) {
					result.remove( e.getKey(), e.getValue() );
				}
			}
			return result;
		}
		
	}
	
	private final int id;
	private final String key;
	
	private final HashMultimap<InventoryAction, ClickType> filterActions = HashMultimap.create();
	private final HashMultimap<InventoryAction, ClickType> blockedActions = HashMultimap.create();
	private final HashMultimap<HandlerType, ItemHandler> handlers = HashMultimap.create();
	
	// private boolean clickable = false;
	private boolean usable = false;
	private boolean droppable = false;
	private boolean pickupable = false;
	// private boolean moveable = false;
	
	private ActionItemParent parent;
	
	private Result useInteractedBlock = Result.DENY, useItemInHand = Result.DENY;
	
	public ActionItem( int id ) {
		this( id, ActionItemAPI.getActionItemManager() );
	}
	
	public ActionItem( int id, ActionItemParent parent ) {
		Preconditions.checkNotNull( parent );
		this.id = id;
		this.key = ActionItemManager.generateMetaKey( id );
		this.parent = parent;
	}
	
	public void setParent( ActionItemParent parent ) {
		if ( parent == null ) {
			this.parent = ActionItemAPI.getActionItemManager();
		} else {
			this.parent = parent;
		}
	}
	
	public ActionItemParent getParent() {
		return parent;
	}
	
	// ****************************************************************
	// Allowed Actions
	@Override
	public Multimap<InventoryAction, ClickType> getRegisteredHandlerActions() {
		return ImmutableMultimap.copyOf( this.filterActions );
	}
	
	@Override
	public Set<ClickType> getRegisteredHandlerActions( InventoryAction action ) {
		return ImmutableSet.copyOf( this.filterActions.get( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearRegisteredHandlerAction() {
		Multimap<InventoryAction, ClickType> old = getRegisteredHandlerActions();
		this.filterActions.clear();
		return old;
	}
	
	@Override
	public Set<ClickType> clearRegisteredHandlerAction( InventoryAction action ) {
		return ImmutableSet.copyOf( this.filterActions.removeAll( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearRegisteredHandlerActionAll( Set<InventoryAction> actions ) {
		Multimap<InventoryAction, ClickType> result = HashMultimap.create();
		for ( InventoryAction action : actions ) {
			result.putAll( action, this.clearRegisteredHandlerAction( action ) );
		}
		return result;
	}
	
	@Override
	public void registerHandlerAction( InventoryAction... actions ) {
		for ( InventoryAction action : actions ) {
			this.filterActions.putAll( action, Arrays.asList( ClickType.values() ) );
		}
	}
	
	@Override
	public void registerHandlerAction( InventoryAction action, ClickType type, ClickType... types ) {
		this.filterActions.put( action, type );
		this.filterActions.putAll( action, Arrays.asList( types ) );
	}
	
	@Override
	public void registerHandlerActionAll( InventoryAction action, Collection<ClickType> types ) {
		this.filterActions.putAll( action, types );
	}
	
	@Override
	public void registerHandlerActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		this.registerHandlerActionAll( multimap.asMap() );
	}
	
	@Override
	public void registerHandlerActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		for ( Entry<InventoryAction, Collection<ClickType>> e : multimap.entrySet() ) {
			this.registerHandlerActionAll( e.getKey(), e.getValue() );
		}
	}
	
	@Override
	public boolean hasRegisteredHandlerAction( InventoryAction action, ClickType type ) {
		return this.filterActions.containsEntry( action, type );
	}
	
	// ****************************************************************
	// Blocked Actions
	@Override
	public Multimap<InventoryAction, ClickType> getBlockedActions() {
		return ImmutableMultimap.copyOf( this.blockedActions );
	}
	
	@Override
	public Set<ClickType> getBlockedActions( InventoryAction action ) {
		return ImmutableSet.copyOf( this.blockedActions.get( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedAction() {
		Multimap<InventoryAction, ClickType> old = this.getBlockedActions();
		this.blockedActions.clear();
		return old;
	}
	
	@Override
	public Set<ClickType> clearBlockedAction( InventoryAction action ) {
		return ImmutableSet.copyOf( this.blockedActions.removeAll( action ) );
	}
	
	@Override
	public Multimap<InventoryAction, ClickType> clearBlockedActionAll( Set<InventoryAction> actions ) {
		Multimap<InventoryAction, ClickType> result = HashMultimap.create();
		for ( InventoryAction action : actions ) {
			result.putAll( action, this.clearBlockedAction( action ) );
		}
		return result;
	}
	
	@Override
	public void blockAction( InventoryAction... actions ) {
		for ( InventoryAction action : actions ) {
			this.blockedActions.putAll( action, Arrays.asList( ClickType.values() ) );
		}
	}
	
	@Override
	public void blockAction( InventoryAction action, ClickType type, ClickType... types ) {
		this.blockedActions.put( action, type );
		this.blockedActions.putAll( action, Arrays.asList( types ) );
	}
	
	@Override
	public void blockActionAll( InventoryAction action, Collection<ClickType> types ) {
		this.blockedActions.putAll( action, types );
	}
	
	@Override
	public void blockActionAll( Multimap<InventoryAction, ClickType> multimap ) {
		this.blockActionAll( multimap.asMap() );
	}
	
	@Override
	public void blockActionAll( Map<InventoryAction, Collection<ClickType>> multimap ) {
		for ( Entry<InventoryAction, Collection<ClickType>> e : multimap.entrySet() ) {
			this.blockActionAll( e.getKey(), e.getValue() );
		}
	}
	
	@Override
	public boolean hasBlockedAction( InventoryAction action, ClickType type ) {
		return this.blockedActions.containsEntry( action, type );
	}
	
	// ****************************************************************
	// Getters
	
	public int getId() {
		return id;
	}
	
	public String getKey() {
		return key;
	}
	
	private <T> Set<ItemHandler> getHandlersOrDefault( HandlerType type ) {
		if ( handlers.containsKey( type ) ) {
			return handlers.get( type );
		} else {
			return Sets.newHashSet( DEFAULT_HANDLER );
		}
	}
	
	public Set<ItemHandler> getHandlers( final HandlerType type ) {
		Preconditions.checkNotNull( type );
		if ( handlers.containsKey( type ) ) {
			return ImmutableSet.copyOf( handlers.get( type ) );
		} else {
			return ImmutableSet.of();
		}
	}
	
	/**
	 * Adds a new Handler and returns all handlers that are overwritten.
	 * 
	 * @param handler
	 *            - to add
	 */
	public void addHandler( ItemHandler handler ) {
		Preconditions.checkNotNull( handler );
		for ( HandlerType t : HandlerType.getTypes( handler ) ) {
			this.handlers.put( t, handler );
		}
	}
	
	/**
	 * Clears a handler from this Item
	 * 
	 * @param handler
	 * @return true if was removed
	 */
	public boolean removeHandler( ItemHandler handler ) {
		Preconditions.checkNotNull( handler );
		boolean result = false;
		if ( handlers.containsKey( handler ) ) {
			for ( HandlerType t : HandlerType.getTypes( handler ) ) {
				result |= this.handlers.remove( t, handler );
			}
		}
		return result;
	}
	
	public void clearAllHandler() {
		handlers.clear();
	}
	
	public void clearHandlers( HandlerType type ) {
		handlers.removeAll( type );
	}
	
	// ****************************************************************
	// Actions
	
	private void invokeHandlers( final HandlerData data, final PreActionItemEvent<?> pre ) {
		for ( ItemHandler handler : ActionItem.this.getHandlersOrDefault( pre.getHandlerType() ) ) {
			try {
				pre.getHandlerType().getCallMethod().invoke( handler, data, pre );
			} catch ( Exception e ) {
				pre.setCancelled( true );
				ActionItemAPI.log( pre.getHandlerType().name() + " Exception\nWhile invoking on handler: "
						+ handler.getClass().getName(), e );
				ActionItemAPI.logInfo( "Pre Event: ", pre.getClass().getName() );
				ActionItemAPI.logInfo( "Type: ", pre.getHandlerType().name() );
				ActionItemAPI.logInfo( "Type-Method: ", pre.getHandlerType().getCallMethod().getName() );
			}
		}
		Bukkit.getScheduler().runTask( ActionItemAPI.getPlugin(), new Runnable() {
			@Override
			public void run() {
				ActionItemEvent<?> post = pre.toPost();
				for ( ItemHandler handler : ActionItem.this.getHandlersOrDefault( post.getHandlerType() ) ) {
					try {
						post.getHandlerType().getCallMethod().invoke( handler, data, post );
					} catch ( Exception e ) {
						ActionItemAPI.log( post.getHandlerType().name() + " Exception\nWhile invoking on handler: "
								+ handler.getClass().getName(), e );
						ActionItemAPI.logInfo( "Pre Event: ", pre.getClass().getName() );
						ActionItemAPI.logInfo( "Post Event: ", post.getClass().getName() );
						ActionItemAPI.logInfo( "Type: ", post.getHandlerType().name() );
						ActionItemAPI.logInfo( "Type-Method: ", post.getHandlerType().getCallMethod().getName() );
					}
				}
			}
		} );
	}
	
	public void onClickPickup( final PreClickEvent event, final InventoryMenu<?, ?, ?> menu ) {
		final HandlerData data = new HandlerData( ActionItem.this, event.getPlayer(), event.getClickedInventory(), menu );
		invokeHandlers( data, event );
	}
	
	public void onUse( final PreUseEvent event, final InventoryMenu<?, ?, ?> menu ) {
		final HandlerData data = new HandlerData( ActionItem.this, event.getPlayer(), event.getPlayer().getInventory(),
				menu );
		invokeHandlers( data, event );
	}
	
	public <T extends Event & Cancellable> void onDrop( final PreDropEvent event, final InventoryMenu<?, ?, ?> menu ) {
		final HandlerData data = new HandlerData( ActionItem.this, event.getPlayer(), event.getPlayer().getInventory(),
				menu );
		invokeHandlers( data, event );
	}
	
	public <T extends Event & Cancellable> void onDrop( final PreDropClickEvent event, final InventoryMenu<?, ?, ?> menu ) {
		final HandlerData data = new HandlerData( ActionItem.this, event.getPlayer(), event.getPlayer().getInventory(),
				menu );
		invokeHandlers( data, event );
	}
	
	public void onPickup( final PrePickupEvent event, final InventoryMenu<?, ?, ?> menu ) {
		final HandlerData data = new HandlerData( ActionItem.this, event.getPlayer(), event.getPlayer().getInventory(),
				menu );
		invokeHandlers( data, event );
	}
	
	public void onMove( final PreMoveEvent event, final InventoryMenu<?, ?, ?> menu ) {
		final HandlerData data = new HandlerData( ActionItem.this, event.getPlayer(), event.getFromInventory(), menu );
		invokeHandlers( data, event );
	}
	
	// ****************************************************************
	// Parent Caller
	
	protected void callClickOnParent( PreClickEvent event ) {
		this.parent.callClickOnItem( event, this );
	}
	
	protected void callUseOnParent( PreUseEvent event ) {
		this.parent.callUseOnItem( event, this );
	}
	
	protected void callDropOnParent( PreDropEvent event ) {
		this.parent.callDropOnItem( event, this );
	}
	
	protected void callDropOnParent( PreDropClickEvent event ) {
		this.parent.callDropOnItem( event, this );
	}
	
	protected void callPickupOnParent( PrePickupEvent event ) {
		this.parent.callPickupOnItem( event, this );
	}
	
	protected void callMoveOnParent( PreMoveEvent event ) {
		this.parent.callMoveOnItem( event, this );
	}
	
	// **************************************************************** // Clickable
	/*
	 * public boolean isClickable() { return clickable; }
	 * 
	 * public void setClickable( boolean clickable ) { this.clickable = clickable; }
	 */
	
	// **************************************************************** // Useable
	
	public boolean isUseable() {
		return usable;
	}
	
	public void setUseable( boolean usable ) {
		this.usable = usable;
	}
	
	// **************************************************************** // Droppable
	
	public boolean isDroppable() {
		return droppable;
	}
	
	public void setDroppable( boolean droppable ) {
		this.droppable = droppable;
	}
	
	// **************************************************************** // Pickupable
	
	public boolean isPickupable() {
		return pickupable;
	}
	
	public void setPickupable( boolean pickupable ) {
		this.pickupable = pickupable;
	}
	
	// **************************************************************** // Pickupable
	/*
	 * public boolean isMoveable() { return moveable; }
	 * 
	 * public void setMoveable( boolean moveable ) { this.moveable = moveable; }
	 */
	
	// ****************************************************************
	// MetaKey Stuff
	
	private ItemMeta addMetaKey( ItemMeta meta ) {
		ActionItemManager.removeMetaKey( meta );
		
		List<String> lore;
		if ( meta.hasLore() ) {
			lore = meta.getLore();
		} else {
			lore = new ArrayList<String>();
		}
		if ( lore.size() == 0 ) {
			lore.add( "" );
		}
		
		int index = lore.size() - 1;
		String line = lore.get( index );
		if ( line.length() > 32 - key.length() ) {
			lore.add( key );
		} else {
			lore.set( index, line + key );
		}
		meta.setLore( lore );
		return meta;
	}
	
	public boolean addMetaKey( ItemStack itemstack ) {
		return itemstack.setItemMeta( addMetaKey( itemstack.getItemMeta() ) );
	}
	
	public void removeItem( ItemStack itemstack ) {
		if ( this.key.equals( ActionItemManager.getMetaKey( itemstack ) ) ) {
			ActionItemManager.removeMetaKey( itemstack );
		}
	}
	
	public Result getUseInteractedBlock() {
		return useInteractedBlock;
	}
	
	public void setUseInteractedBlock( Result useInteractedBlock ) {
		Validate.notNull( useInteractedBlock );
		this.useInteractedBlock = useInteractedBlock;
	}
	
	public Result getUseItemInHand() {
		return useItemInHand;
	}
	
	public void setUseItemInHand( Result useItemInHand ) {
		Validate.notNull( useItemInHand );
		this.useItemInHand = useItemInHand;
	}
	
	@Override
	public String toString() {
		return getKey();
	}
	
}
