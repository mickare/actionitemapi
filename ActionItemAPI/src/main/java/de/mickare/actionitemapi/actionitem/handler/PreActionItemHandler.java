package de.mickare.actionitemapi.actionitem.handler;

public interface PreActionItemHandler extends PreClickHandler, PreDropHandler, PreDropClickHandler, PrePickupHandler, PreUseHandler,
		PreMoveHandler, ItemHandler {
	
}
