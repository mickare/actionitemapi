package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PostClickEvent;

public interface PostClickHandler extends ItemHandler {
	
	public void onPostClick( HandlerData data, PostClickEvent event );
	
}
