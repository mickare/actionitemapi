package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PreMoveEvent;

public interface PreMoveHandler extends ItemHandler {
	
	public void onPreMove( HandlerData data, PreMoveEvent event );
	
}
