package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PostDropEvent;

public interface PostDropHandler extends ItemHandler {
	
	public void onPostDrop( HandlerData data, PostDropEvent event );
	
}
