package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PrePickupEvent;

public interface PrePickupHandler extends ItemHandler {

	public void onPrePickup( HandlerData data, PrePickupEvent event );

}
