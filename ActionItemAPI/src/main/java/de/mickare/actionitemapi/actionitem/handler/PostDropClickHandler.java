package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PostDropClickEvent;

public interface PostDropClickHandler extends ItemHandler {

	public void onPostDrop( HandlerData data, PostDropClickEvent event );
	
}
