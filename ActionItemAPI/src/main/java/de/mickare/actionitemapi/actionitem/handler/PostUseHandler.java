package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PostUseEvent;

public interface PostUseHandler extends ItemHandler {

	public void onPostUse( HandlerData data, PostUseEvent event );

}
