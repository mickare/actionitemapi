package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PreDropEvent;

public interface PreDropHandler extends ItemHandler {
	
	public void onPreDrop( HandlerData data, PreDropEvent event );
	
}
