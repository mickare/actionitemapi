package de.mickare.actionitemapi.actionitem;

import de.mickare.actionitemapi.events.*;

public interface ActionItemParent {
	
	void callClickOnItem( PreClickEvent event, ActionItem item );
	
	void callUseOnItem( PreUseEvent event, ActionItem item );
	
	void callDropOnItem( PreDropEvent event, ActionItem item );
	
	void callDropOnItem( PreDropClickEvent event, ActionItem item );
	
	void callPickupOnItem( PrePickupEvent event, ActionItem item );
	
	void callMoveOnItem( PreMoveEvent event, ActionItem item );
	
}
