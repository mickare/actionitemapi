package de.mickare.actionitemapi.actionitem.handler;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.inventory.InventoryMenu;

public class HandlerData {

	private ActionItem actionitem;
	private InventoryMenu<?, ?, ?> menu;
	private Player player;
	private Inventory inventory;
	
	public HandlerData(ActionItem actionitem, Player player, Inventory inventory) {
		this(actionitem, player, inventory,  null);
	}
	
	public HandlerData(ActionItem actionitem, Player player, Inventory inventory, InventoryMenu<?, ?, ?> menu) {
		this.actionitem = actionitem;
		this.menu = menu;
		this.player = player;
		this.inventory = inventory;
	}

	/**
	 * Get the involved inventory menu.
	 * @return null if no menu involved
	 */
	public InventoryMenu<?, ?, ?> getMenu() {
		return menu;
	}

	/**
	 * Is an inventory menu involved
	 * @return true if inventory menu is involved
	 */
	public boolean hasMenu() {
		return menu != null;
	}
	
	/**
	 * Get the ActionItem of the event.
	 * @return
	 */
	public ActionItem getActionItem() {
		return actionitem;
	}

	/**
	 * Get the involved Player
	 * @return Player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Get the involved inventory
	 * @return inventory
	 */
	public Inventory getInventory() {
		return inventory;
	}

	public void clear() {
		this.actionitem = null;
		this.menu = null;
		this.player = null;
		this.inventory = null;
	}
	
	
}
