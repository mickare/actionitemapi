package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PostMoveEvent;

public interface PostMoveHandler extends ItemHandler {
	
	public void onPostMove( HandlerData data, PostMoveEvent event );
	
}
