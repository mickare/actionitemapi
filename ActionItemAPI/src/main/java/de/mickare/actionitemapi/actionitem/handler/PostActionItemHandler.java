package de.mickare.actionitemapi.actionitem.handler;

public interface PostActionItemHandler extends PostClickHandler, PostDropHandler, PostDropClickHandler,
		PostPickupHandler, PostUseHandler, PostMoveHandler, ItemHandler {
	
}
