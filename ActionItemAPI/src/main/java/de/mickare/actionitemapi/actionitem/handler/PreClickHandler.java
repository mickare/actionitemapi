package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PreClickEvent;

public interface PreClickHandler extends ItemHandler {
	
	public void onPreClick( HandlerData data, PreClickEvent event );
	
}
