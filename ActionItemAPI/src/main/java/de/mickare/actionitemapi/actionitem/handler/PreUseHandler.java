package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PreUseEvent;

public interface PreUseHandler extends ItemHandler {

	public void onPreUse( HandlerData data, PreUseEvent event );

}
