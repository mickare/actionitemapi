package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PreDropClickEvent;

public interface PreDropClickHandler extends ItemHandler {
		
	public void onPreDrop( HandlerData data, PreDropClickEvent event );
	
}
