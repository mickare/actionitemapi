package de.mickare.actionitemapi.actionitem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import de.mickare.actionitemapi.ActionItemAPI;
import static de.mickare.actionitemapi.ActionItemAPI.logInfo;
import de.mickare.actionitemapi.api.EventListenerDeactivatable;
import de.mickare.actionitemapi.events.PreClickEvent;
import de.mickare.actionitemapi.events.PreDropClickEvent;
import de.mickare.actionitemapi.events.PreDropEvent;
import de.mickare.actionitemapi.events.PreMoveEvent;
import de.mickare.actionitemapi.events.PrePickupEvent;
import de.mickare.actionitemapi.events.PreUseEvent;
import de.mickare.actionitemapi.utils.ActionItemAPIUtils;

public class ActionItemManager implements Listener, EventListenerDeactivatable, ActionItemParent {
	
	// *******************************************************
	// Static
	
	protected static final Pattern ITEMACTION_PATTERN = Pattern.compile( "(?i)" + String.valueOf( ChatColor.COLOR_CHAR )
			+ "[M]" + String.valueOf( ChatColor.COLOR_CHAR ) + "[N]" + "(" + String.valueOf( ChatColor.COLOR_CHAR )
			+ "[0-9A-F])+$" );
	
	public static boolean hasMetaKey( ItemStack itemstack ) {
		if ( itemstack.hasItemMeta() ) {
			return hasMetaKey( itemstack.getItemMeta() );
		}
		return false;
	}
	
	public static boolean hasMetaKey( ItemMeta meta ) {
		if ( meta != null ) {
			if ( meta.hasLore() ) {
				List<String> lore = meta.getLore();
				if ( lore != null ) {
					if ( lore.size() > 0 ) {
						return ITEMACTION_PATTERN.matcher( lore.get( lore.size() - 1 ) ).find();
					}
				}
			}
		}
		return false;
	}
	
	public static void removeMetaKey( ItemStack itemstack ) {
		if ( itemstack.hasItemMeta() ) {
			itemstack.setItemMeta( removeMetaKey( itemstack.getItemMeta() ) );
		}
	}
	
	protected static ItemMeta removeMetaKey( ItemMeta meta ) {
		if ( ActionItemManager.hasMetaKey( meta ) ) {
			List<String> lore = meta.getLore();
			if ( lore.size() > 0 ) {
				lore.set( lore.size() - 1, ActionItemManager.ITEMACTION_PATTERN.matcher( lore.get( lore.size() - 1 ) )
						.replaceAll( "" ) );
			}
		}
		return meta;
	}
	
	public static String getMetaKey( ItemStack itemstack ) {
		if ( itemstack.hasItemMeta() ) {
			return getMetaKey( itemstack.getItemMeta() );
		}
		return null;
	}
	
	public static String getMetaKey( ItemMeta meta ) {
		if ( meta != null ) {
			if ( meta.hasLore() ) {
				List<String> lore = meta.getLore();
				if ( lore.size() > 0 ) {
					Matcher m = ITEMACTION_PATTERN.matcher( lore.get( lore.size() - 1 ) );
					if ( m.find() ) {
						return m.group();
					}
				}
			}
		}
		return null;
	}
	
	public static Map<Integer, ItemStack> removeItemsFromInventory( int metaid, Inventory inventory ) {
		return removeItemsFromInventory( generateMetaKey( metaid ), inventory );
	}
	
	public static Map<Integer, ItemStack> removeItemsFromInventory( ActionItem actionitem, Inventory inventory ) {
		return removeItemsFromInventory( actionitem.getKey(), inventory );
	}
	
	public static Map<Integer, ItemStack> removeItemsFromInventory( String metakey, Inventory inventory ) {
		Map<Integer, ItemStack> result = getItemsFromInventory( metakey, inventory );
		for ( int index : result.keySet() ) {
			inventory.clear( index );
		}
		return result;
	}
	
	public static Map<Integer, ItemStack> getItemsFromInventory( int metaid, Inventory inventory ) {
		return getItemsFromInventory( generateMetaKey( metaid ), inventory );
	}
	
	public static Map<Integer, ItemStack> getItemsFromInventory( ActionItem actionitem, Inventory inventory ) {
		return getItemsFromInventory( actionitem.getKey(), inventory );
	}
	
	public static Map<Integer, ItemStack> getItemsFromInventory( String metakey, Inventory inventory ) {
		HashMap<Integer, ItemStack> result = new HashMap<Integer, ItemStack>();
		for ( int index = 0; index < inventory.getSize(); index++ ) {
			ItemStack is = inventory.getItem( index );
			if ( is != null ) {
				if ( !is.getType().equals( Material.AIR ) ) {
					if ( metakey.equals( getMetaKey( is ) ) ) {
						result.put( index, is );
					}
				}
			}
		}
		return result;
	}
	
	public static String generateMetaKey( int metaid ) {
		StringBuilder sb = new StringBuilder();
		sb.append( ChatColor.STRIKETHROUGH ).append( ChatColor.UNDERLINE );
		sb.append( ActionItemAPIUtils.convertToColors( Integer.toString( metaid, 16 ) ) );
		return sb.toString();
	}
	
	// *******************************************************
	// Unstatic
	
	private static final int MAX_ID = ( int ) Math.pow( 16, 6 );
	
	private boolean activatedListener = false;
	
	// <Key, ItemAction>
	private final Map<String, ActionItem> actions = new HashMap<String, ActionItem>();
	
	private final Plugin plugin;
	
	public ActionItemManager( Plugin plugin ) {
		this.plugin = plugin;
	}
	
	public boolean isActivatedListener() {
		return activatedListener;
	}
	
	public void setActivatedListener( boolean activatedListener ) {
		this.activatedListener = activatedListener;
	}
	
	public ActionItem createActionItem( int id ) {
		return new ActionItem( id );
	}
	
	public int getFreeId() {
		return getFreeId( 0 );
	}
	
	public int getFreeId( int start ) {
		int id = start;
		Preconditions.checkArgument( id < MAX_ID );
		while ( id < MAX_ID ) {
			if ( isFreeId( id ) ) {
				return id;
			}
			id++;
		}
		return -1;
	}
	
	public boolean isFreeId( int id ) {
		return !actions.containsKey( generateMetaKey( id ) );
	}
	
	public void registerActionItem( ActionItem actionitem ) {
		if ( actions.containsKey( actionitem.getKey() ) ) {
			ActionItemAPI.logInfoLevel( 2, "[WARNING] Overriding ActiomItem (", actionitem.getId(), ") with new one!" );
		}
		actions.put( actionitem.getKey(), actionitem );
	}
	
	public void unregisterActionItem( ActionItem actionitem ) {
		actions.remove( actionitem.getKey() );
	}
	
	public void unregisterActionItem( int id ) {
		actions.remove( generateMetaKey( id ) );
	}
	
	public void unregisterAllActionItems() {
		actions.clear();
	}
	
	public void registerAllActionItems( Collection<ActionItem> c ) {
		for ( ActionItem ai : c ) {
			registerActionItem( ai );
		}
	}
	
	public ActionItem getActionItem( String key ) {
		return actions.get( key );
	}
	
	public ActionItem getActionItem( ItemStack itemstack ) {
		if ( itemstack == null || itemstack.getType() == Material.AIR ) {
			return null;
		}
		if ( itemstack.hasItemMeta() ) {
			return getActionItem( itemstack.getItemMeta() );
		}
		return null;
	}
	
	public ActionItem getActionItem( ItemMeta meta ) {
		String key = getMetaKey( meta );
		if ( key != null ) {
			return actions.get( key );
		} else {
			return null;
		}
	}
	
	public ActionItem[] getAllActionItems() {
		return actions.values().toArray( new ActionItem[0] );
	}
	
	public ActionItem[] getAllActionItemsOfClass( Class<?> cls ) {
		HashSet<ActionItem> result = new HashSet<ActionItem>();
		for ( ActionItem ai : actions.values() ) {
			if ( ai.getClass().isAssignableFrom( cls ) ) {
				
			}
		}
		return result.toArray( new ActionItem[0] );
	}
	
	private static final void log( final String name, final ItemStack item ) {
		if ( item != null ) {
			logInfo( name + item.getType().toString() + " x " + item.getAmount() );
		} else {
			logInfo( name + "none" );
		}
	}
	
	// ******************************************************************************
	// Listener Inventory Clicks
	// ******************************************************************************
	
	private static final Map<UUID, ItemStack> DRAGGED_ITEM = Maps.newHashMap();
	private static final Map<UUID, Inventory> DRAGGED_ORIGIN = Maps.newHashMap();
	
	private static final synchronized int cursorPickup( Player player, ItemStack item, Inventory inventory ) {
		int amountPickedUp = item.getAmount();
		if ( DRAGGED_ITEM.containsKey( player.getUniqueId() ) ) {
			ItemStack old = DRAGGED_ITEM.get( player.getUniqueId() );
			if ( old.isSimilar( item ) ) {
				int free = old.getMaxStackSize() - old.getAmount();
				amountPickedUp = item.getAmount() < free ? item.getAmount() : free;
				old.setAmount( old.getAmount() + amountPickedUp );
			}
		}
		DRAGGED_ITEM.put( player.getUniqueId(), item.clone() );
		DRAGGED_ORIGIN.put( player.getUniqueId(), inventory );
		return amountPickedUp;
	}
	
	private static final synchronized Inventory cursorDropAll( Player player, ItemStack item ) {
		Inventory result = null;
		ItemStack old = DRAGGED_ITEM.get( player.getUniqueId() );
		if ( old != null && old.equals( item ) ) {
			result = DRAGGED_ORIGIN.get( player.getUniqueId() );
		}
		DRAGGED_ITEM.remove( player.getUniqueId() );
		DRAGGED_ORIGIN.remove( player.getUniqueId() );
		return result;
	}
	
	private static final synchronized Inventory cursorDropOne( Player player, ItemStack item ) {
		Inventory result = null;
		ItemStack old = DRAGGED_ITEM.get( player.getUniqueId() );
		if ( old != null && old.equals( item ) ) {
			if ( old.getAmount() == 1 ) {
				return cursorDropAll( player, item );
			}
			old.setAmount( old.getAmount() - 1 );
			result = DRAGGED_ORIGIN.get( player.getUniqueId() );
		}
		return result;
	}
	
	/*
	 * private static final synchronized Inventory cursorPlaceSome( Player player, ItemStack item, Inventory
	 * targetInventory, ItemStack target ) { int free = targetInventory.getMaxStackSize(); if ( target != null &&
	 * target.getType() != Material.AIR ) { free = target.getMaxStackSize() - target.getAmount(); }
	 * 
	 * Inventory result = null; ItemStack old = DRAGGED_ITEM.get( player.getUniqueId() ); if ( old != null &&
	 * old.equals( item ) ) { if ( old.getAmount() <= free ) { return cursorDropAll( player, item ); } old.setAmount(
	 * old.getAmount() - free ); result = DRAGGED_ORIGIN.get( player.getUniqueId() ); } return result; }
	 */
	
	@EventHandler( priority = EventPriority.HIGH, ignoreCancelled = true )
	public void onInventoryClick( InventoryClickEvent event ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		logInfo( "+--------------------------" );
		logInfo( "> InventoryClickEvent" );
		logInfo( "| Cancelled: " + event.isCancelled() );
		logInfo( "| SlotType: " + event.getSlotType() );
		
		logInfo( "| InventoryAction: " + event.getAction() );
		logInfo( "| ClickType: " + event.getClick().toString() );
		
		boolean oldCancel = event.isCancelled();
		
		// No Items! ... All personal jump from board!
		if ( event.getCurrentItem() == null && event.getCursor() == null ) {
			return;
		}
		
		// Only Players
		if ( !( event.getWhoClicked() instanceof Player ) ) {
			return;
		}
		
		logInfo( "| WhoClicked: " + event.getWhoClicked().getName() );
		final Player player = ( Player ) event.getWhoClicked();
		
		if ( player.getGameMode().equals( GameMode.CREATIVE ) ) {
			// Disabled in GameMode!
			return;
		}
		
		final ActionItem clicked = this.getActionItem( event.getCurrentItem() );
		final ActionItem cursor = this.getActionItem( event.getCursor() );
		final ActionItem hotbar = event.getClick() == ClickType.NUMBER_KEY ? this.getActionItem( player.getInventory()
				.getItem( event.getHotbarButton() ) ) : null;
		
		log( "| Clicked Item: ", event.getCurrentItem() );
		log( "| Cursor Item: ", event.getCursor() );
		
		if ( event.getClick() == ClickType.NUMBER_KEY ) {
			log( "| Hotbar Item: ", player.getInventory().getItem( event.getHotbarButton() ) );
		}
		
		if ( clicked == null && cursor == null && hotbar == null ) {
			// No Action Items
			return;
		}
		
		if ( clicked != null ) {
			logInfo( "| Clicked ActionItem: " + clicked.getId() );
		}
		if ( cursor != null ) {
			logInfo( "| Cursor ActionItem: " + cursor.getId() );
		}
		if ( hotbar != null ) {
			logInfo( "| Horbar ActionItem: " + hotbar.getId() );
		}
		
		if ( clicked != null && !clicked.hasRegisteredHandlerAction( event.getAction(), event.getClick() ) ) {
			logInfo( "| Clicked ActionItem denied Action" );
			event.setCancelled( true );
			// return;
		}
		if ( cursor != null && !cursor.hasRegisteredHandlerAction( event.getAction(), event.getClick() ) ) {
			logInfo( "| Cursor ActionItem denied Action" );
			event.setCancelled( true );
			// return;
		}
		if ( hotbar != null && !hotbar.hasRegisteredHandlerAction( event.getAction(), event.getClick() ) ) {
			logInfo( "| Hotbar ActionItem denied Action" );
			event.setCancelled( true );
			// return;
		}
		if ( event.isCancelled() == oldCancel ) {
			switch ( event.getAction() ) {
				case CLONE_STACK:
					if ( cursor != null ) {
						cursorPickup( player, event.getCurrentItem().clone(), event.getClickedInventory() );
					}
					break;
				case COLLECT_TO_CURSOR:
					if ( cursor != null ) {
						cursorPickup( player, ActionItemAPIUtils.collectAll( event.getView(), event.getCurrentItem() ), event
								.getClickedInventory() );
					}
					break;
				case DROP_ALL_CURSOR:
					if ( cursor != null ) {
						cursor.callDropOnParent( new PreDropClickEvent( event, cursor, player, event.getCursor(),
								cursorDropAll( player, event.getCursor() ) ) );
					}
					break;
				case DROP_ALL_SLOT:
					if ( cursor != null ) {
						cursor.callDropOnParent( new PreDropClickEvent( event, cursor, player, event.getCurrentItem(),
								event.getClickedInventory() ) );
					}
					break;
				case DROP_ONE_CURSOR:
					if ( cursor != null ) {
						cursor.callDropOnParent( new PreDropClickEvent( event, cursor, player, event.getCursor(),
								cursorDropOne( player, event.getCursor() ) ) );
					}
					break;
				case DROP_ONE_SLOT:
					if ( cursor != null ) {
						cursor.callDropOnParent( new PreDropClickEvent( event, cursor, player, event.getCurrentItem(),
								event.getClickedInventory() ) );
					}
					break;
				case MOVE_TO_OTHER_INVENTORY:
					if ( clicked != null ) {
						Inventory from = event.getClickedInventory();
						Inventory to = event.getView().getTopInventory();
						if ( to == from ) {
							to = event.getView().getBottomInventory();
						}
						clicked.callMoveOnParent( new PreMoveEvent( event, clicked, player, event.getCurrentItem(),
								from, event.getView().convertSlot( event.getRawSlot() ), to, to.firstEmpty() ) );
					}
					break;
				case HOTBAR_MOVE_AND_READD:
					if ( clicked != null ) {
						clicked.callMoveOnParent( new PreMoveEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), event.getView().convertSlot( event.getRawSlot() ), player
										.getInventory(), event.getHotbarButton() ) );
					}
					if ( hotbar != null ) {
						ItemStack hotbarItem = player.getInventory().getItem( event.getHotbarButton() );
						hotbar.callMoveOnParent( new PreMoveEvent( event, hotbar, player, hotbarItem, player
								.getInventory(), event.getHotbarButton(), player.getInventory(), ActionItemAPIUtils
								.firstStackableSlot( player.getInventory(), hotbarItem, event.getHotbarButton() ) ) );
					}
					break;
				case HOTBAR_SWAP:
					if ( clicked != null ) {
						clicked.callMoveOnParent( new PreMoveEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), event.getView().convertSlot( event.getRawSlot() ), player
										.getInventory(), event.getHotbarButton() ) );
					}
					if ( hotbar != null ) {
						ItemStack hotbarItem = player.getInventory().getItem( event.getHotbarButton() );
						hotbar.callMoveOnParent( new PreMoveEvent( event, hotbar, player, hotbarItem, player
								.getInventory(), event.getHotbarButton(), event.getClickedInventory(), event.getView()
								.convertSlot( event.getRawSlot() ) ) );
					}
					break;
				case NOTHING:
					break;
				case PICKUP_ALL:
					if ( clicked != null ) {
						int amount = cursorPickup( player, event.getCurrentItem(), event.getClickedInventory() );
						clicked.callClickOnParent( new PreClickEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), amount ) );
					}
					break;
				case PICKUP_HALF:
					if ( clicked != null ) {
						ItemStack pickup = event.getCurrentItem().clone();
						pickup.setAmount( pickup.getAmount() / 2 );
						int amount = cursorPickup( player, event.getCurrentItem(), event.getClickedInventory() );
						
						clicked.callClickOnParent( new PreClickEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), amount ) );
						
					}
					break;
				case PICKUP_ONE:
					if ( clicked != null ) {
						ItemStack pickup = event.getCurrentItem().clone();
						pickup.setAmount( 1 );
						int amount = cursorPickup( player, pickup, event.getClickedInventory() );
						
						clicked.callClickOnParent( new PreClickEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), amount ) );
					}
					break;
				case PICKUP_SOME:
					if ( clicked != null && cursor != null ) {
						// ItemStack pickup = event.getCurrentItem().clone();
						// pickup.setAmount( 1 );
						int amount = cursorPickup( player, event.getCurrentItem(), event.getClickedInventory() );
						
						clicked.callClickOnParent( new PreClickEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), amount ) );
					}
					break;
				case PLACE_ALL:
					if ( cursor != null ) {
						Inventory from = cursorDropAll( player, event.getCursor() );
						cursor.callMoveOnParent( new PreMoveEvent( event, cursor, player, event.getCursor(), from, -1,
								event.getClickedInventory(), event.getView().convertSlot( event.getRawSlot() ) ) );
					}
					break;
				case PLACE_ONE:
					if ( cursor != null ) {
						Inventory from = cursorDropOne( player, event.getCursor() );
						ItemStack itemPlaced = event.getCursor().clone();
						itemPlaced.setAmount( 1 );
						cursor.callMoveOnParent( new PreMoveEvent( event, cursor, player, itemPlaced, from, -1, event
								.getClickedInventory(), event.getView().convertSlot( event.getRawSlot() ) ) );
					}
					break;
				case PLACE_SOME:
					if ( cursor != null ) {
						Inventory from = cursorDropAll( player, event.getCursor() );
						int slot = event.getView().convertSlot( event.getRawSlot() );
						ItemStack itemPlaced = event.getCursor().clone();
						ItemStack target = event.getClickedInventory().getItem( slot );
						if ( target != null ) {
							int free = target.getMaxStackSize() - target.getAmount();
							itemPlaced.setAmount( itemPlaced.getAmount() < free ? itemPlaced.getAmount() : free );
						}
						cursor.callMoveOnParent( new PreMoveEvent( event, cursor, player, event.getCursor(), from, -1,
								event.getClickedInventory(), slot ) );
					}
					break;
				case SWAP_WITH_CURSOR:
					if ( cursor != null ) {
						Inventory from = cursorDropAll( player, event.getCursor() );
						cursor.callMoveOnParent( new PreMoveEvent( event, cursor, player, event.getCursor(), from, -1,
								event.getClickedInventory(), event.getView().convertSlot( event.getRawSlot() ) ) );
					}
					if ( clicked != null ) {
						int amount = cursorPickup( player, event.getCurrentItem(), event.getClickedInventory() );
						clicked.callClickOnParent( new PreClickEvent( event, clicked, player, event.getCurrentItem(),
								event.getClickedInventory(), amount ) );
					}
					break;
				case UNKNOWN:
				default:
					break;
			}
		}
		
		if ( clicked != null && clicked.hasBlockedAction( event.getAction(), event.getClick() ) ) {
			logInfo( "| Clicked ActionItem blocked Action" );
			event.setCancelled( true );
		}
		if ( cursor != null && cursor.hasBlockedAction( event.getAction(), event.getClick() ) ) {
			logInfo( "| Cursor ActionItem blocked Action" );
			event.setCancelled( true );
		}
		
		if ( hotbar != null && hotbar.hasBlockedAction( event.getAction(), event.getClick() ) ) {
			logInfo( "| Hotbar ActionItem blocked Action" );
			event.setCancelled( true );
		}
		
		if ( event.isCancelled() != oldCancel ) {
			Bukkit.getScheduler().runTask( plugin, new Runnable() {
				@Override
				public void run() {
					player.updateInventory();
				}
			} );
		}
		
		logInfo( "| Event Cancelled: ", event.isCancelled() );
		logInfo( "<" );
	}
	
	// ******************************************************************************
	// Listener Inventory Drag
	// ******************************************************************************
	
	@EventHandler( priority = EventPriority.HIGH, ignoreCancelled = true )
	public void onInventoryDrag( InventoryDragEvent event ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		
		logInfo( "+--------------------------" );
		logInfo( "> InventoryDragEvent" );
		logInfo( "| Cancelled: " + event.isCancelled() );
		logInfo( "| DragType: " + event.getType().toString() );
		
		// Only Players
		if ( !( event.getWhoClicked() instanceof Player ) ) {
			return;
		}
		
		logInfo( "| WhoClicked: " + event.getWhoClicked().getName() );
		
		// No Item! ... All personal jump from board!
		if ( event.getOldCursor() == null ) {
			return;
		}
		
		log( "| OldCursor: ", event.getOldCursor() );
		
		// final ActionItem oldCursor = this.getActionItem( event.getOldCursor() );
		
		final ActionItem oldCursor = this.getActionItem( event.getOldCursor() );
		if ( oldCursor != null ) {
			// Just deny all of them!
			// TODO - for the future with more time!
			event.setResult( Result.DENY );
			logInfo( "| Event denied - ActionItem ID : " + oldCursor.getId() );
		}
		
		/*
		 * logInfo( "Cursor: " + event.getCursor().getType().toString() + " x " + event.getCursor().getAmount() );
		 */
		
		logInfo( "| Event Cancelled: ", event.isCancelled() );
		logInfo( "<" );
	}
	
	// ******************************************************************************
	// Listener Use
	// ******************************************************************************
	
	@EventHandler( priority = EventPriority.HIGH, ignoreCancelled = false )
	public void onPlayerUse( final PlayerInteractEvent event ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		
		logInfo( "+--------------------------" );
		logInfo( "> PlayerInteractEvent" );
		logInfo( "| Cancelled: " + event.isCancelled() );
		
		boolean oldCancel = event.isCancelled();
		
		ItemStack itemstack = event.getItem();
		if ( itemstack == null ) {
			return;
		}
		
		logInfo( "| Item: " + itemstack.getType().toString() + " x " + itemstack.getAmount() );
		
		ActionItem ia = this.getActionItem( itemstack );
		
		if ( ia != null ) {
			logInfo( "| ActionItem: " + ia.getId() );
			// logInfo( "| Useable: " + ia.isUseable() );
			
			if ( Result.DENY.equals( ia.getUseInteractedBlock() ) ) {
				event.setUseInteractedBlock( Result.DENY );
			}
			
			if ( Result.DENY.equals( ia.getUseItemInHand() ) ) {
				event.setUseItemInHand( Result.DENY );
			}
			
			if ( !ia.isUseable() ) {
				logInfo( "| Not useable!" );
				event.setCancelled( true );
			}
			
			ia.callUseOnParent( new PreUseEvent( event, ia, event.getPlayer(), event.getPlayer().getInventory()
					.getHeldItemSlot() ) );
			
		}
		
		if ( event.isCancelled() != oldCancel ) {
			Bukkit.getScheduler().runTask( plugin, new Runnable() {
				@Override
				public void run() {
					event.getPlayer().updateInventory();
				}
			} );
		}
		
		logInfo( "| Event Cancelled: ", event.isCancelled() );
		logInfo( "<" );
	}
	
	// ******************************************************************************
	// Listener Drop
	// ******************************************************************************
	
	@EventHandler( priority = EventPriority.HIGH, ignoreCancelled = true )
	public void onPlayerDropItem( PlayerDropItemEvent event ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		
		logInfo( "+--------------------------" );
		logInfo( "> PlayerDropItemEvent" );
		logInfo( "| Cancelled: " + event.isCancelled() );
		
		ItemStack itemstack = event.getItemDrop().getItemStack();
		
		if ( itemstack == null ) {
			return;
		}
		
		logInfo( "| Item: " + itemstack.getType().toString() + " x " + itemstack.getAmount() );
		
		ActionItem ia = this.getActionItem( itemstack );
		if ( ia != null ) {
			logInfo( "| ActionItem: " + ia.getId() );
			
			if ( !ia.isDroppable() ) {
				logInfo( "| Not droppable!" );
				event.setCancelled( true );
			}
			
			// logInfo( "| Droppable: " + ia.isDroppable() );
			ia.callDropOnParent( new PreDropEvent( event, ia, event.getPlayer(), event.getItemDrop().getItemStack(),
					event.getPlayer().getInventory() ) );
		}
		logInfo( "| Event Cancelled: ", event.isCancelled() );
		logInfo( "<" );
	}
	
	// ******************************************************************************
	// Listener Pickup
	// ******************************************************************************
	
	@EventHandler( priority = EventPriority.HIGH, ignoreCancelled = true )
	public void onPlayerPickUpItem( PlayerPickupItemEvent event ) {
		if ( !this.isActivatedListener() ) {
			return;
		}
		
		logInfo( "+--------------------------" );
		logInfo( "> PlayerPickupItemEvent" );
		
		ItemStack itemstack = event.getItem().getItemStack();
		
		if ( itemstack == null ) {
			return;
		}
		
		logInfo( "| Item: " + itemstack.getType().toString() + " x " + itemstack.getAmount() );
		
		ActionItem ia = this.getActionItem( itemstack );
		if ( ia != null ) {
			logInfo( "| ActionItem: " + ia.getId() );
			
			if ( !ia.isPickupable() ) {
				logInfo( "| Not pickupable!" );
				event.setCancelled( true );
			}
			
			// logInfo( "| Pickupable: " + ia.isPickupable() );
			ia.callPickupOnParent( new PrePickupEvent( event, ia, event.getPlayer(), event.getItem().getItemStack() ) );
		}
		
		/*
		 * if ( event.isCancelled() ) { Bukkit.getScheduler().runTask( plugin, new Runnable() {
		 * 
		 * @Override public void run() { event.getPlayer().updateInventory(); } } ); }
		 */
		logInfo( "<" );
	}
	
	// ******************************************************************************
	// Listener Dragged Items Remover
	// ******************************************************************************
	
	@EventHandler( priority = EventPriority.HIGHEST, ignoreCancelled = false )
	public void onPlayerInventoryClose( InventoryCloseEvent event ) {
		DRAGGED_ITEM.remove( event.getPlayer().getUniqueId() );
		DRAGGED_ORIGIN.remove( event.getPlayer().getUniqueId() );
	}
	
	@EventHandler( priority = EventPriority.HIGHEST, ignoreCancelled = false )
	public void onPlayerQuit( PlayerQuitEvent event ) {
		DRAGGED_ITEM.remove( event.getPlayer().getUniqueId() );
		DRAGGED_ORIGIN.remove( event.getPlayer().getUniqueId() );
	}
	
	// ******************************************************************************
	// Manager simple callbacks
	// ******************************************************************************
	
	@Override
	public void callClickOnItem( PreClickEvent event, ActionItem item ) {
		item.onClickPickup( event, null );
	}
	
	@Override
	public void callUseOnItem( PreUseEvent event, ActionItem item ) {
		item.onUse( event, null );
	}
	
	@Override
	public void callDropOnItem( PreDropEvent event, ActionItem item ) {
		item.onDrop( event, null );
	}
	
	@Override
	public void callDropOnItem( PreDropClickEvent event, ActionItem item ) {
		item.onDrop( event, null );
	}
	
	@Override
	public void callPickupOnItem( PrePickupEvent event, ActionItem item ) {
		item.onPickup( event, null );
	}
	
	@Override
	public void callMoveOnItem( PreMoveEvent event, ActionItem item ) {
		item.onMove( event, null );
	}
	
}
