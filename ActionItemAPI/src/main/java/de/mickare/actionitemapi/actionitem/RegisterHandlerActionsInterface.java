package de.mickare.actionitemapi.actionitem;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;

import com.google.common.collect.Multimap;

public interface RegisterHandlerActionsInterface {
	
	Multimap<InventoryAction, ClickType> getRegisteredHandlerActions();
	
	Set<ClickType> getRegisteredHandlerActions( InventoryAction action );
	
	Multimap<InventoryAction, ClickType> clearRegisteredHandlerAction();
	
	Set<ClickType> clearRegisteredHandlerAction( InventoryAction action );
	
	Multimap<InventoryAction, ClickType> clearRegisteredHandlerActionAll( Set<InventoryAction> actions );
	
	void registerHandlerAction( InventoryAction... actions );
	
	void registerHandlerAction( InventoryAction action, ClickType type, ClickType... types );
	
	void registerHandlerActionAll( InventoryAction action, Collection<ClickType> types );
	
	void registerHandlerActionAll( Multimap<InventoryAction, ClickType> multimap );
	
	void registerHandlerActionAll( Map<InventoryAction, Collection<ClickType>> multimap );
	
	boolean hasRegisteredHandlerAction( InventoryAction action, ClickType type );
	
}
