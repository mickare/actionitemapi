package de.mickare.actionitemapi.actionitem.handler;

import de.mickare.actionitemapi.events.PostPickupEvent;

public interface PostPickupHandler extends ItemHandler {

	public void onPostPickup( HandlerData data, PostPickupEvent event );

}
