package de.mickare.actionitemapi.actionitem.handler;

import java.lang.reflect.Method;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Sets;

public enum HandlerType {
	POST_CLICK( PostClickHandler.class ),
	POST_USE( PostUseHandler.class ),
	POST_DROP( PostDropHandler.class ),
	POST_DROPCLICK( PostDropClickHandler.class ),
	POST_PICKUP( PostPickupHandler.class ),
	POST_MOVE( PostMoveHandler.class ),
	PRE_CLICK( PreClickHandler.class ),
	PRE_USE( PreUseHandler.class ),
	PRE_DROP( PreDropHandler.class ),
	PRE_DROPCLICK( PreDropClickHandler.class ),
	PRE_PICKUP( PrePickupHandler.class ),
	PRE_MOVE( PreMoveHandler.class );
	
	private final Class<? extends ItemHandler> handlerClass;
	private final Method callMethod;
	
	private HandlerType( Class<? extends ItemHandler> handlerClass ) {
		this.handlerClass = handlerClass;
		this.callMethod = handlerClass.getDeclaredMethods()[0];
	}
	
	public Class<? extends ItemHandler> getHandlerClass() {
		return handlerClass;
	}
	
	private static final BiMap<Class<? extends ItemHandler>, HandlerType> map;
	
	static {
		ImmutableBiMap.Builder<Class<? extends ItemHandler>, HandlerType> mapBuilder = ImmutableBiMap.builder();
		for ( HandlerType t : HandlerType.values() ) {
			mapBuilder.put( t.getHandlerClass(), t );
		}
		map = mapBuilder.build();
	}
	
	public static Set<HandlerType> getTypes( ItemHandler handler ) {
		Set<HandlerType> result = Sets.newHashSet();
		for ( Entry<Class<? extends ItemHandler>, HandlerType> e : map.entrySet() ) {
			if ( e.getKey().isInstance( handler ) ) {
				result.add( e.getValue() );
			}
		}
		return result;
	}
	
	public Method getCallMethod() {
		return callMethod;
	}
	
}
