package de.mickare.actionitemapi;

import java.util.Arrays;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public final class LoreUtils {
	
	private LoreUtils() {
	}
	
	public static ItemStack setDisplayName( final ItemStack is, final String displayName ) {
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName( displayName );
		is.setItemMeta( meta );
		return is;
	}
	
	public static ItemStack setLore( final ItemStack is, final String... lore ) {
		return setLore( is, Arrays.asList( lore ) );
	}
	
	public static ItemStack setLore( final ItemStack is, final List<String> lore ) {
		ItemMeta meta = is.getItemMeta();
		meta.setLore( lore );
		is.setItemMeta( meta );
		return is;
	}
	
	public static ItemStack setDisplayNameAndLore( final ItemStack is, final String displayName, final String... lore ) {
		return setDisplayNameAndLore( is, displayName, Arrays.asList( lore ) );
	}
	
	public static ItemStack setDisplayNameAndLore( final ItemStack is, final String displayName, final List<String> lore ) {
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName( displayName );
		meta.setLore( lore );
		is.setItemMeta( meta );
		return is;
	}
	
}
