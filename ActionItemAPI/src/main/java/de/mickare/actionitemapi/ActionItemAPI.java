package de.mickare.actionitemapi;

import java.util.logging.Level;

import org.apache.commons.lang.Validate;

import de.mickare.actionitemapi.actionitem.ActionItemManager;
import de.mickare.actionitemapi.inventory.custom.CustomInventoryManager;
import de.mickare.actionitemapi.inventory.player.PlayerInventoryManager;
import de.mickare.actionitemapi.utils.ActionItemAPIUtils;

public class ActionItemAPI {
	
	private static boolean static_debugging = false;
	private static int static_debugLevel = 0;
	
	private static ActionItemAPIPlugin plugin = null;
	
	protected static void setInstance( ActionItemAPIPlugin instance ) {
		Validate.notNull( instance );
		if ( plugin != null ) {
			throw new IllegalStateException( "ItemActionAPI already initialized" );
		}
		plugin = instance;
	}
	
	public static ActionItemAPIPlugin getPlugin() {
		return plugin;
	}
	
	protected static void removeInstance() {
		if ( plugin != null ) {
			plugin.onRemoveInstance();
			plugin = null;
		}
	}
	
	public static ActionItemManager getActionItemManager() {
		Validate.notNull( plugin, "ItemActionAPI not initialized" );
		return plugin.getActionItemManager();
	}
	
	public static PlayerInventoryManager getPlayerInventoryManager() {
		Validate.notNull( plugin, "ItemActionAPI not initialized" );
		return plugin.getPlayerInventoryManager();
	}
	
	public static CustomInventoryManager getCustomInventoryManager() {
		Validate.notNull( plugin, "ItemActionAPI not initialized" );
		return plugin.getCustomInventoryManager();
	}
	
	public static int getFreeId() {
		return getActionItemManager().getFreeId();
	}
	
	public static int getFreeId( int start ) {
		return getActionItemManager().getFreeId( start );
	}
	
	public static boolean isFreeId( int id ) {
		return getActionItemManager().isFreeId( id );
	}
	
	public static final boolean isDebugging() {
		return static_debugging;
	}
	
	public static void setDebugging( boolean debugging ) {
		static_debugging = debugging;
	}
	
	public static int getDebugLevel() {
		return static_debugLevel;
	}
	
	public static void setDebugLevel( int debugLevel ) {
		ActionItemAPI.static_debugLevel = debugLevel;
	}
	
	public static final void log( String msg, Exception e ) {
		plugin.getLogger().log( Level.SEVERE, msg + "\n" + e.getMessage(), e );
	}
	
	public static final void logInfo( final Object... msg ) {
		logInfoLevel( 0, msg );
	}
	
	public static final void logInfoLevel( final int level, final Object... msg ) {
		if ( static_debugLevel >= level && isDebugging() && plugin != null ) {
			StringBuilder m = new StringBuilder();
			for ( Object o : msg ) {
				m.append( o != null ? o.toString() : "null" );
			}
			plugin.getLogger().info( m.toString()
					+ ( static_debugLevel >= 4 ? "\n"
							+ ActionItemAPIUtils.stackTraceToString( Thread.currentThread().getStackTrace() ) : "" ) );
		}
	}
	
}
