package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public class PrePickupEvent extends PreActionItemEvent<PlayerPickupItemEvent> {
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private final ItemStack pickedUpItemStack;
	
	public PrePickupEvent( PlayerPickupItemEvent event, ActionItem actionitem, Player player,
			ItemStack pickedUpItemStack ) {
		super( event, actionitem, player, HandlerType.PRE_PICKUP );
		this.pickedUpItemStack = pickedUpItemStack;
		ActionItemAPI.logInfo( ">> PrePickupEvent" );
	}
	
	public ItemStack getPickedUpItemStack() {
		return pickedUpItemStack;
	}
	
	@Override
	public PostPickupEvent toPost() {
		return new PostPickupEvent( this.getEvent(), this.getActionitem(), this.getPlayer(),
				this.getPickedUpItemStack() );
	}
	
}
