package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public class PostDropClickEvent extends ActionItemEvent<InventoryClickEvent> {
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private final ItemStack itemStack;
	private final Inventory from;
	
	public PostDropClickEvent( InventoryClickEvent event, ActionItem actionitem, Player player, ItemStack itemStack, Inventory from ) {
		super( event, actionitem, player, HandlerType.POST_DROPCLICK );
		this.itemStack = itemStack;
		this.from = from;
		ActionItemAPI.logInfo( ">> PostDropEvent" );
	}
	
	public Inventory getFrom() {
		return from;
	}
	
	public ItemStack getItemStack() {
		return itemStack;
	}
	
}
