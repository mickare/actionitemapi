package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public abstract class ActionItemEvent<E extends Event> extends Event {
	
	public abstract HandlerList getHandlers();
	
	private final E event;
	private final ActionItem actionitem;
	private final Player player;
	
	private final HandlerType handlerType;
	
	public ActionItemEvent( E event, ActionItem actionitem, Player player, HandlerType handlerType ) {
		this.event = event;
		this.actionitem = actionitem;
		this.player = player;
		this.handlerType = handlerType;
	}
	
	public ActionItem getActionitem() {
		return actionitem;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public E getEvent() {
		return event;
	}
	
	public HandlerType getHandlerType() {
		return handlerType;
	}
}
