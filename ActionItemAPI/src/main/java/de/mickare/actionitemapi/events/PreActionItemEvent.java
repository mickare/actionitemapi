package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public abstract class PreActionItemEvent<E extends Event & Cancellable> extends ActionItemEvent<E> implements
		Cancellable {
	
	public PreActionItemEvent( E event, ActionItem actionitem, Player player, HandlerType handlerType ) {
		super( event, actionitem, player, handlerType );
		/*
		 * if ( event1 != event2 ) { throw new IllegalArgumentException( "Event and Cancellable are not the same!" ); }
		 */
		// this.event2 = event2;
	}
	
	@Override
	public boolean isCancelled() {
		return this.getEvent().isCancelled();
	}
	
	@Override
	public void setCancelled( boolean cancel ) {
		this.getEvent().setCancelled( cancel );
	}
	
	public abstract ActionItemEvent<E> toPost();
	
}
