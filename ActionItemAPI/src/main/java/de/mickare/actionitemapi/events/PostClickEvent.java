package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public class PostClickEvent extends ActionItemEvent<InventoryClickEvent> {
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private final Inventory clickedInventory;
	private final ItemStack clickedItem;
	private final int amountPickedUp;
	
	public PostClickEvent( InventoryClickEvent event, ActionItem actionitem, Player player,
			ItemStack clickedItem, Inventory clickedInventory, int amountPickedUp ) {
		super( event, actionitem, player, HandlerType.POST_CLICK );
		this.clickedInventory = clickedInventory;
		this.clickedItem = clickedItem;
		this.amountPickedUp = amountPickedUp;
		ActionItemAPI.logInfo( ">> PostClickEvent" );
	}
	
	public Inventory getClickedInventory() {
		return clickedInventory;
	}
	
	public ItemStack getClickedItem() {
		return clickedItem;
	}
	
	public int getAmountPickedUp() {
		return amountPickedUp;
	}
	
}
