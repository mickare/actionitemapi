package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public class PostMoveEvent extends ActionItemEvent<InventoryClickEvent> {
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private final InventoryClickEvent event;
	
	private final Inventory fromInventory, toInventory;
	private final ItemStack item;
	// fromSlot can be -1, when the place where it came from isn't clear. (Inventory Item Move via Cursor)
	private final int fromSlot, toSlot;
	
	public PostMoveEvent( InventoryClickEvent event, ActionItem actionitem, Player player, ItemStack item,
			Inventory fromInventory, int fromSlot, Inventory toInventory, int toSlot ) {
		super( event, actionitem, player, HandlerType.POST_MOVE );
		this.event = event;
		this.item = item;
		this.fromInventory = fromInventory;
		this.fromSlot = fromSlot;
		this.toInventory = toInventory;
		this.toSlot = toSlot;
		ActionItemAPI.logInfo( ">> PostMoveEvent" );
	}
	
	@Override
	public InventoryClickEvent getEvent() {
		return event;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public Inventory getFromInventory() {
		return fromInventory;
	}
	
	public Inventory getToInventory() {
		return toInventory;
	}
	
	public int getFromSlot() {
		return fromSlot;
	}
	
	public int getToSlot() {
		return toSlot;
	}
	
}
