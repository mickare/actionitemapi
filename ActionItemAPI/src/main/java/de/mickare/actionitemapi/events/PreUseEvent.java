package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerInteractEvent;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public class PreUseEvent extends PreActionItemEvent<PlayerInteractEvent> {
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private final int heldItemSlot;
	
	public PreUseEvent( PlayerInteractEvent event, ActionItem actionitem, Player player, int heldItemSlot ) {
		super( event, actionitem, player, HandlerType.PRE_USE );
		this.heldItemSlot = heldItemSlot;
		ActionItemAPI.logInfo( ">> PreUseEvent" );
	}
	
	public int getHeldItemSlot() {
		return heldItemSlot;
	}
	
	@Override
	public PostUseEvent toPost() {
		return new PostUseEvent( this.getEvent(), this.getActionitem(), this.getPlayer(), this.getHeldItemSlot() );
	}
	
}
