package de.mickare.actionitemapi.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.ActionItemAPI;
import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.handler.HandlerType;

public class PreDropEvent extends PreActionItemEvent<PlayerDropItemEvent> {
	
	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private final ItemStack itemStack;
	private final Inventory from;
	
	public PreDropEvent( PlayerDropItemEvent event, ActionItem actionitem, Player player, ItemStack itemStack,
			Inventory from ) {
		super( event, actionitem, player, HandlerType.PRE_DROP );
		this.itemStack = itemStack;
		this.from = from;
		ActionItemAPI.logInfo( ">> PreDropEvent" );
	}
	
	public Inventory getFrom() {
		return from;
	}
	
	public ItemStack getItemStack() {
		return itemStack;
	}
	
	@Override
	public PostDropEvent toPost() {
		return new PostDropEvent( this.getEvent(), this.getActionitem(), this.getPlayer(), this.getItemStack(),
				this.getFrom() );
	}
	
}
