package de.mickare.actionitemapi;

import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.actionitem.ActionItemManager;
import de.mickare.actionitemapi.inventory.custom.CustomInventoryManager;
import de.mickare.actionitemapi.inventory.player.PlayerInventoryManager;

public class ActionItemAPIPlugin extends JavaPlugin {

	// *************************
	// Managers

	ActionItemManager actionItemManager = null;
	PlayerInventoryManager playerInventoryManager = null;
	CustomInventoryManager custominventoryManager = null;

	@Override
	public void onDisable() {
		onRemoveInstance();
		ActionItemAPI.removeInstance();
	}

	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		ActionItemAPI.setDebugging(this.getConfig().getBoolean("debugging", false));
		ActionItemAPI.setDebugLevel( this.getConfig().getInt( "debugLevel", 0) );
		
		onRemoveInstance();

		actionItemManager = new ActionItemManager( this );
		playerInventoryManager = new PlayerInventoryManager( this );
		custominventoryManager = new CustomInventoryManager( this );

		getServer().getPluginManager().registerEvents( actionItemManager, this );
		getServer().getPluginManager().registerEvents( playerInventoryManager, this );
		getServer().getPluginManager().registerEvents( custominventoryManager, this );

		ActionItemAPI.setInstance( this );

		actionItemManager.setActivatedListener( true );
		playerInventoryManager.setActivatedListener( true );
		custominventoryManager.setActivatedListener( true );

	}

	public ActionItemManager getActionItemManager() {
		return actionItemManager;
	}

	public PlayerInventoryManager getPlayerInventoryManager() {
		return playerInventoryManager;
	}

	public CustomInventoryManager getCustomInventoryManager() {
		return custominventoryManager;
	}

	public void onRemoveInstance() {
		if (actionItemManager != null) {
			actionItemManager.setActivatedListener( false );
		}
		if (playerInventoryManager != null) {
			playerInventoryManager.setActivatedListener( false );
		}
		if (custominventoryManager != null) {
			custominventoryManager.setActivatedListener( false );
		}
		actionItemManager = null;
		playerInventoryManager = null;
		custominventoryManager = null;
	}

}
