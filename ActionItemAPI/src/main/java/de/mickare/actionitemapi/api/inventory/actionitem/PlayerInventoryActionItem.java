package de.mickare.actionitemapi.api.inventory.actionitem;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.inventory.IPlayerInventoryMenu;

public class PlayerInventoryActionItem extends ActionItem {
	
	private final IPlayerInventoryMenu menu;
	
	public PlayerInventoryActionItem( int id, IPlayerInventoryMenu menu ) {
		super( id, menu );
		this.menu = menu;
	}
	
	public IPlayerInventoryMenu getMenu() {
		return menu;
	}
	
	// ****************************************************************
	// allowedInventoryActions - Overwrite
	
	/*
	 * private static final HashSet<InventoryAction> inventoryActions_that_are_ok = new HashSet<InventoryAction>(
	 * Arrays.asList( InventoryAction.NOTHING ) );
	 * 
	 * @Override public void addInventoryAction( InventoryAction ia ) { Validate.isTrue(
	 * inventoryActions_that_are_ok.contains( ia ), "Not allowed Inventory Action for PlayerInventory-ActionItems!" );
	 * // throw IllegalArgumentException(); // super.addInventoryAction(ia); }
	 */
	
}
