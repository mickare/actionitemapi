package de.mickare.actionitemapi.api.inventory;

import org.bukkit.inventory.Inventory;

import de.mickare.actionitemapi.api.inventory.actionitem.CustomInventoryActionItem;

public interface ICustomInventoryMenu extends InventoryMenu<ICustomInventoryManager, ICustomInventoryMenu, CustomInventoryActionItem> {

	Inventory getInventory();

}
