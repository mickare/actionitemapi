package de.mickare.actionitemapi.api.inventory;

import java.util.Set;

import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.actionitem.ActionItem;

public interface InventoryManager<M extends InventoryManager<M, I, A>, I extends InventoryMenu<M, I, A>, A extends ActionItem> {

	void registerInventory(I menu);

	void unregisterInventory(I menu);

	Set<I> getMenues();

	JavaPlugin getPlugin();

	M getSelf();

	boolean isActivatedListener();

	void setActivatedListener(boolean activatedListener);

}
