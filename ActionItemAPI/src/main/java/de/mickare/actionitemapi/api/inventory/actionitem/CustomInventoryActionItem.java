package de.mickare.actionitemapi.api.inventory.actionitem;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.api.inventory.ICustomInventoryMenu;

public class CustomInventoryActionItem extends ActionItem {

	private final ICustomInventoryMenu menu;

	public CustomInventoryActionItem(int id, ICustomInventoryMenu menu) {
		super( id, menu );
		this.menu = menu;
	}

	public ICustomInventoryMenu getMenu() {
		return menu;
	}

}
