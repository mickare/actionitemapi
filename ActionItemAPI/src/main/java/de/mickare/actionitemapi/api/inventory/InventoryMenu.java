package de.mickare.actionitemapi.api.inventory;

import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.actionitem.ActionItem;
import de.mickare.actionitemapi.actionitem.ActionItemParent;
import de.mickare.actionitemapi.actionitem.RegisterHandlerActionsInterface;
import de.mickare.actionitemapi.actionitem.BlockedActionsInterface;

public interface InventoryMenu<M extends InventoryManager<M, I, A>, I extends InventoryMenu<M, I, A>, A extends ActionItem>
		extends RegisterHandlerActionsInterface, ActionItemParent, BlockedActionsInterface {
	
	A _newItem( int id );
	
	A newItem( int id );
	
	A newItem( int id, boolean register );
	
	A newItem( int id, ItemStack... itemstacks );
	
	A newItem( int id, boolean register, ItemStack... itemstacks );
	
	A setItem( int index, int id, ItemStack itemstack );
	
	A setItem( int index, A item, ItemStack itemstack );
	
	void setItem( int index, ItemStack itemstack );
	
	void removeItem( ActionItem item );
	
	void removeItem( String metakey );
	
	void removeItem( int metaid );
	
	boolean contains( String itemkey );
	
	boolean contains( ActionItem item );
	
	int count( ActionItem actionItem );
	
	A[] getItems();
	
	void clear();
	
	void clear( int index );
	
	I getSelf();
	
	JavaPlugin getPlugin();
	
	M getManager();
	
	A getActionItem( int metaid );
	
	A getActionItem( String metakey );
	
	void addActionItem( A actionitem );
	
	A removeActionItem( int metaid );
	
	A removeActionItem( String metakey );
	
	A removeActionItem( ActionItem actionitem );
	
	void clearActionItems();
	
	Set<A> getActionItems();
	
	boolean hasActionItem( int metaid );
	
	boolean hasActionItem( String metakey );
	
	boolean hasActionItem( ActionItem actionitem );
	
	void clearItemsWithMetaKey();
	
	void clearItemsWithMetaKey( int metaid_start, int metaid_end );
	
	void openInventoryTo( Player player );
	
}
