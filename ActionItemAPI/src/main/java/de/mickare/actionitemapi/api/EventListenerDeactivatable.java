package de.mickare.actionitemapi.api;

import org.bukkit.event.Listener;

public interface EventListenerDeactivatable extends Listener {
	
	public boolean isActivatedListener();

	public void setActivatedListener( boolean activatedListener );
	
}
