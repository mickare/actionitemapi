package de.mickare.actionitemapi.api.inventory;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.java.JavaPlugin;

import de.mickare.actionitemapi.api.inventory.actionitem.CustomInventoryActionItem;

public interface ICustomInventoryManager extends InventoryManager<ICustomInventoryManager, ICustomInventoryMenu, CustomInventoryActionItem> {
	
	ICustomInventoryMenu createMenu( JavaPlugin plugin, InventoryHolder owner, int lines, String title );

	ICustomInventoryMenu createMenu( JavaPlugin plugin, Inventory inventory );
	
}
