package de.mickare.actionitemapi.api.inventory;

import org.bukkit.entity.Player;

import de.mickare.actionitemapi.api.inventory.actionitem.PlayerInventoryActionItem;

public interface IPlayerInventoryManager extends InventoryManager<IPlayerInventoryManager, IPlayerInventoryMenu, PlayerInventoryActionItem> {

	void removePlayerFromAll(Player player);

	IPlayerInventoryMenu createMenu(boolean doReplaceRendering);

}
