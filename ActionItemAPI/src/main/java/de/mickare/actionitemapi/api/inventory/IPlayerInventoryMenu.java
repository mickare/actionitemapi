package de.mickare.actionitemapi.api.inventory;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.mickare.actionitemapi.api.inventory.actionitem.PlayerInventoryActionItem;
import de.mickare.actionitemapi.inventory.player.handler.RenderHandler;

public interface IPlayerInventoryMenu extends InventoryMenu<IPlayerInventoryManager, IPlayerInventoryMenu, PlayerInventoryActionItem> {

	String[] getPlayers();

	boolean isDoReplaceRendering();

	void setDoReplaceRendering(boolean doReplaceRendering);

	void setItem(int index, ItemStack itemstack, boolean doReplaceRendering);

	boolean hasPlayer(Player player);

	boolean hasPlayer(UUID uuid);

	void addPlayer(Player player);

	void removePlayer(Player player);

	public void renderItemStackToPlayer(Player player, int index,
			ItemStack itemstack);

	public void renderItemStackToPlayer(Player player, int index,
			ItemStack itemstack, boolean doReplaceRendering);

	public void renderAllItemStacksToPlayer(Player player);

	public void renderAllItemStacksToPlayer(Player player,
			boolean doReplaceRendering);

	public void renderItemStackToAllPlayers(int index, ItemStack itemstack);

	public void renderItemStackToAllPlayers(int index, ItemStack itemstack,
			boolean doReplaceRendering);

	public void removeAllWithMetaIdFromAllPlayer(int metaid);

	public void removeAllWithMetaIdFromPlayer(Player player, int metaid);

	public void removeAllWithMetaKeyFromAllPlayer(String metakey);

	public void removeAllWithMetaKeyFromPlayer(Player player, String metakey);

	public void removeItemStackFromPlayer(Player player, int index,
			ItemStack itemstack);

	public void removeAllItemStacksFromPlayer(Player player);

	public void removeItemStackFromAllPlayers(int index, ItemStack itemstack);

	public void renderAllItemStacksToPlayerInventory(Inventory inv,
			boolean doReplaceRendering);

	public RenderHandler getOnRenderHandler();

	public void setOnRenderHandler(RenderHandler onRenderHandler);


}
